using Android.App;
using Android.Widget;

namespace Daily.MonoDroid.Services
{
    public class PopupFactory : IPopupFactory
    {
        public void Show(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }
    }

}

