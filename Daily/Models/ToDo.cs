﻿using System;

namespace Daily.Models
{
    public class ToDo : ModelBase
    {
        public DateTime NotifyDateTime
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int AlertId
        {
            get;
            set;
        }

        #region Frequency

        public bool Sunday
        {
            get;
            set;
        }

        public bool Monday
        {
            get;
            set;
        }

        public bool Tuesday
        {
            get;
            set;
        }

        public bool Wednesday
        {
            get;
            set;
        }

        public bool Thursday
        {
            get;
            set;
        }

        public bool Friday
        {
            get;
            set;
        }

        public bool Saturday
        {
            get;
            set;
        }

        public int DaysOn
        {
            get;
            set;
        }

        public int DaysOff
        {
            get;
            set;
        }

        #endregion
    
        #region Color
        public string Color
        {
            get;
            set;
        }
        #endregion
    
        #region AlertSound
        public string AlertSoundPath
        {
            get;
            set;
        }
        #endregion
    }

}

