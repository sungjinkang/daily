﻿using System;

namespace Daily.Web.Services
{
    public class PopupFactory : IPopupFactory
    {
        Guid userId;
        public PopupFactory(Guid userId)
        {
            this.userId = userId;
        }

        #region IPopupFactory implementation

        public void Show(string message)
        {
            Console.WriteLine("user {0} encountered an error {1}", userId, message);
        }

        #endregion
    }
}

