﻿using System;
using Android.Content;
using Android.Views;
using Android.Util;
using Android.Widget;
using Daily.Core;
using System.Collections.Generic;
using Daily.Core.ViewModels;

namespace Daily.MonoDroid.Views
{
    public class DayView : DayViewBase
    {
        public const int HourHeight = 100;

        LinearLayout toDoViews;

        LockableScrollView lockableScrollView;

        public event EventHandler Changed;
        public event EventHandler<GuidEventArgs> Edit;

        public DayView(Context context)
            : base(context)
        {
            Initialize();
        }

        public DayView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public DayView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            lockableScrollView = new LockableScrollView(Context)
            {
                LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                    ViewGroup.LayoutParams.MatchParent),
            };

            var layoutParams = new ScrollView.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                                   ViewGroup.LayoutParams.WrapContent);
            layoutParams.BottomMargin = 100;
            toDoViews = new LinearLayout(Context)
            {
                LayoutParameters = new ScrollView.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                    ViewGroup.LayoutParams.WrapContent),
                Orientation = Orientation.Vertical,
            };
            toDoViews.SetPadding(0, 0, 0, 100);
            lockableScrollView.AddView(toDoViews);
            AddView(lockableScrollView);
        }



        void InvokeEdit(object sender, GuidEventArgs e)
        {
            var edit = Edit;
            if (edit != null)
            {
                edit(sender, e);
            }
        }

        protected override void UpdateToDoViews(IList<ToDoViewModel> toDoViewModels)
        {
            var count = toDoViews.ChildCount;
            for (int i = 0; i < count; i++)
            {
                var todoView = toDoViews.GetChildAt(i) as ToDoView;
                if (todoView != null)
                {
                    todoView.ClearListeners();
                }
            }
            toDoViews.RemoveAllViews();
            foreach (var vm in toDoViewModels)
            {
                var layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                                       ViewGroup.LayoutParams.WrapContent);
                var toDoView = new ToDoView(Context)
                {
                    LayoutParameters = layoutParams,
                    ViewModel = vm,
                    Date = ViewModel.Date,
                };
                toDoView.Edit += (sender, e) =>
                {
                    InvokeEdit(sender, e);
                };
        
                toDoView.Changed += (sender, e) =>
                {
                    var changed = Changed;
                    if (changed != null)
                    {
                        changed(this, e);
                    }
                };
        
                toDoView.DismissStart += (sender, e) =>
                {
                    lockableScrollView.LockScroll = true;
                };
                toDoView.DismissEnd += (sender, e) =>
                {
                    lockableScrollView.LockScroll = false;
                };
        
                var didDone = new DidDoneView(Context)
                {
                    LayoutParameters = layoutParams,
                    ViewModel = vm,
                };
                didDone.Click += (sender, e) => InvokeEdit(didDone, new GuidEventArgs(vm.ToDoId));
                var container = new RelativeLayout(Context);
                container.AddView(didDone);
                container.AddView(toDoView);
                toDoView.BringToFront();
        
                toDoViews.AddView(container);
            }
        }
    }
}

