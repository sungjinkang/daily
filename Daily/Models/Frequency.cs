using System;

namespace Daily.Models
{

    public enum Frequency
    {
        Daily,
        Weekly,
        Intermittent
    }
    
}
