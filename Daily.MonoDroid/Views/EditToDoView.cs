﻿using System;
using System.Linq;
using Android.Content;
using Android.Util;
using Android.Widget;
using Android.Views;
using Android.Graphics;
using Daily.Models;
using Daily.Core.ViewModels;
using Android.Media;
using System.Collections.Generic;
using Android.App;
using Daily.MonoDroid.Adapters;
using System.Threading.Tasks;
using Android.OS;
using Android.Runtime;

namespace Daily.MonoDroid.Views
{
    public class EditToDoView : ToDoViewBase
    {
        TimePickerView notifyTime;
        EditText title;
        EditText description;
        EditText daysOn;
        EditText daysOff;
        TextView alertSound;
        TextView frequency;
        ViewGroup editViewContainer;
        ViewGroup editView;
        ViewGroup intermittentContainer;
        ViewGroup weeklyContainer;
        DayOfWeekPickerView dayOfWeekPicker;
        ColorPickerView colorPicker;
        MediaPlayer mediaPlayer;
        LockableScrollView lockableScrollView;

        public EditToDoView(Context context)
            : base(context)
        {
            Initialize();
        }

        public EditToDoView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public EditToDoView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            Inflate(Context, Resource.Layout.EditToDoView, this);

            lockableScrollView = FindViewById<LockableScrollView>(Resource.Id.LockableScrollView);

            #region ToDo
            editViewContainer = FindViewById<ViewGroup>(Resource.Id.EditViewContainer);
            editView = FindViewById<ViewGroup>(Resource.Id.EditView);
            notifyTime = FindViewById<TimePickerView>(Resource.Id.NotifyTime);
            title = FindViewById<EditText>(Resource.Id.Title);
            description = FindViewById<EditText>(Resource.Id.Description);
            notifyTime.TimeChanged += OnNotifyTimeChanged;
            notifyTime.ChangeTimeStart += delegate
            {
                lockableScrollView.LockScroll = true;
            };
            notifyTime.ChangeTimeEnd += delegate
            {
                lockableScrollView.LockScroll = false;
            };
            title.TextChanged += OnTitleTextChanged;
            description.TextChanged += OnDescriptionTextChanged;
            #endregion

            #region Frequency
            frequency = FindViewById<TextView>(Resource.Id.Frequency);
            frequency.Click += delegate
            {
                var adapter = new RadioListAdapter<Frequency>(ViewModel.FrequencyAdapter);

                adapter.CurrentPosition = ViewModel.SelectedFrequencyIndex;
                var dialog = new AlertDialog.Builder(Context)
                        .SetAdapter(adapter, (s, e) =>
                    {
                        ViewModel.SelectedFrequencyIndex = e.Which;
                    })
                        .Create();
                dialog.VolumeControlStream = Stream.Alarm;
                dialog.Show();
            };
            #endregion
            
            #region Weekly
            weeklyContainer = FindViewById<ViewGroup>(Resource.Id.WeeklyContainer);
            dayOfWeekPicker = FindViewById<DayOfWeekPickerView>(Resource.Id.DayOfWeekPicker);
            dayOfWeekPicker.DayChecked += OnDayOfWeekPickerDayChecked;

            #endregion

            #region Intermittent
            intermittentContainer = FindViewById<ViewGroup>(Resource.Id.IntermittentContainer);
            daysOn = FindViewById<EditText>(Resource.Id.DaysOn);
            daysOff = FindViewById<EditText>(Resource.Id.DaysOff);

            daysOn.TextChanged += OnDaysOnTextChanged;
            daysOff.TextChanged += OnDaysOffTextChanged;
            #endregion

            #region Color
            colorPicker = FindViewById<ColorPickerView>(Resource.Id.ColorPicker);
            colorPicker.ColorChanged += OnColorPickerColorChanged;
            colorPicker.ChangeColorStart += delegate
            {
                lockableScrollView.LockScroll = true;
            };
            colorPicker.ChangeColorEnd += delegate
            {
                lockableScrollView.LockScroll = false;
            };
            #endregion

            #region AlertSound
            alertSound = FindViewById<TextView>(Resource.Id.AlertSound);
            #endregion

        }

        void UpdateFrequencyContainers()
        {
            intermittentContainer.Visibility = ViewStates.Gone;
            weeklyContainer.Visibility = ViewStates.Gone;
            switch ((Frequency)ViewModel.SelectedFrequencyIndex)
            {
                case Frequency.Daily:
                    break;
                case Frequency.Weekly:
                    weeklyContainer.Visibility = ViewStates.Visible;
                    break;
                case Frequency.Intermittent:
                    intermittentContainer.Visibility = ViewStates.Visible;
                    break;
            }
            frequency.Text = ViewModel.FrequencyAdapter.Get(ViewModel.SelectedFrequencyIndex).ToString();

            UpdateWeekContainer();
            UpdateDaysOnOff();
        }

        void UpdateColor(string color)
        {
            var c = color.ToColor();
            UpdateBackgroundColors(c);
            colorPicker.Color = c;
        }

        void PopulateMediaData()
        {
            var adapter = new RadioListAdapter<IAlert>(ViewModel.AlertsAdapter);
            alertSound.Click += delegate
            {
                var listView = new ListView(Context)
                {
                    Adapter = adapter,
                };
                listView.ItemClick += (sender, e) =>
                {
                    if (mediaPlayer != null)
                    {
                        mediaPlayer.Stop();
                    }
                    adapter.CurrentPosition = e.Position;
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.SetAudioStreamType(Stream.Alarm);
                    var uri = ((MediaData)ViewModel.AlertsAdapter.Get(e.Position)).Uri;
                    mediaPlayer.SetDataSource(Context, uri);
                    mediaPlayer.Looping = true;
                    mediaPlayer.Prepare();
                    mediaPlayer.Start();
                    adapter.NotifyDataSetChanged();
                };
                var dialog = new AlertDialog.Builder(Context)
                                .SetView(listView)
                                .SetNegativeButton("Cancel", delegate
                    {
                        if (mediaPlayer != null)
                        {
                            mediaPlayer.Stop();
                        }
                    })
                    .SetPositiveButton("Set", delegate
                    {
                        ViewModel.SelectedAlertIndex = adapter.CurrentPosition;
                        var selectedItem = ViewModel.AlertsAdapter.Get(adapter.CurrentPosition);
                        if (mediaPlayer != null)
                        {
                            mediaPlayer.Stop();
                        }
                    })
                    .Create();
                dialog.VolumeControlStream = Stream.Alarm;
                dialog.Show();
            };
        }

        void OnColorPickerColorChanged(object sender, ColorPickerView.ColorChangeEventArgs e)
        {
            ViewModel.Color = e.Color.ToHexString();
            UpdateBackgroundColors(e.Color);
        }

        protected void UpdateBackgroundColors(Color color)
        {
            var background = editViewContainer.Background;
            background.SetColorFilter(color, PorterDuff.Mode.Multiply);
            editViewContainer.SetBackgroundDrawable(background);
            editView.SetBackgroundColor(color);
        }

        void OnDayOfWeekPickerDayChecked(object sender, DayOfWeekPickerView.DayCheckEventArgs e)
        {
            ViewModel.ToggleDayOfWeek = e.DayOfWeek;
        }

        void OnDaysOffTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            int result = 0;
            int.TryParse(e.Text.ToString(), out result);
            ViewModel.DaysOff = result;
        }

        void OnDaysOnTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            int result = 0;
            int.TryParse(e.Text.ToString(), out result);
            ViewModel.DaysOn = result;
        }

        void OnNotifyTimeChanged(object sender, EventArgs e)
        {
            var timePicker = sender as TimePickerView;
            if (timePicker != null)
            {
                ViewModel.NotifyDateTime = timePicker.Time;
            }
        }

        void OnDescriptionTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            ViewModel.Description = description.Text;
        }

        void OnTitleTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            ViewModel.Title = title.Text;
        }

        protected override void OnViewModelChanged(ToDoViewModel value)
        {
            base.OnViewModelChanged(value);
            notifyTime.Time = value.NotifyDateTime;
            title.Text = value.Title;
            description.Text = value.Description;
            UpdateColor(value.Color);
            UpdateAlertSound();
            UpdateFrequencyContainers();
            PopulateMediaData();
        }

        protected override void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedFrequencyIndex")
            {
                UpdateFrequencyContainers();
            }
            else if (e.PropertyName == "SelectedAlertIndex")
            {
                UpdateAlertSound();
            }
        }

        void UpdateAlertSound()
        {
            var ringtoneManager = new RingtoneManager(Context);
            var alert = ViewModel.AlertsAdapter.Get(ViewModel.SelectedAlertIndex);

            if (string.IsNullOrEmpty(alert.Path))
            {
                alertSound.Text = "No Alarm";
            }
            else
            {
                var pos = ringtoneManager.GetRingtonePosition(Android.Net.Uri.Parse(alert.Path));
                alertSound.Text = ringtoneManager.GetRingtone(pos).GetTitle(Context);
            }
        }

        void UpdateWeekContainer()
        {
            for (int i = 0; i < 7; i++)
            {
                dayOfWeekPicker.Week[i].Checked = ViewModel.DayOfWeekAdapter.Get(i).Value;
            }
        }

        void UpdateDaysOnOff()
        {
            daysOn.Text = ViewModel.DaysOn.ToString();
            daysOff.Text = ViewModel.DaysOff.ToString();
        }


        protected override IParcelable OnSaveInstanceState()
        {
            var editToDoViewSavedState = new EditToDoViewSavedState(base.OnSaveInstanceState())
            {
                SelectedAlertIndex = ViewModel.SelectedAlertIndex,
                SelectedFrequencyIndex = ViewModel.SelectedFrequencyIndex,
            };

            return editToDoViewSavedState;
        }

        protected override void OnRestoreInstanceState(IParcelable state)
        {
            var editToDoViewSavedState = state as EditToDoViewSavedState;
            if (editToDoViewSavedState == null)
            {
                base.OnRestoreInstanceState(state);
                return;
            }
            base.OnRestoreInstanceState(editToDoViewSavedState.SuperState);
            ViewModel.SelectedAlertIndex = editToDoViewSavedState.SelectedAlertIndex;
            ViewModel.SelectedFrequencyIndex = editToDoViewSavedState.SelectedFrequencyIndex;
        }

        class EditToDoViewSavedState : BaseSavedState
        {
            public EditToDoViewSavedState(Parcel source)
                : base(source)
            {
            
            }

            public EditToDoViewSavedState(IParcelable superState)
                : base(superState)
            {
                
            }

            public EditToDoViewSavedState(IntPtr javaReference, JniHandleOwnership transfer)
                : base(javaReference, transfer)
            {
                
            }

            public override void WriteToParcel(Parcel dest, ParcelableWriteFlags flags)
            {
                base.WriteToParcel(dest, flags);
                dest.WriteInt(SelectedAlertIndex);
                dest.WriteInt(SelectedFrequencyIndex);
            }

            public int SelectedAlertIndex
            {
                get;
                set;
            }

            public int SelectedFrequencyIndex
            {
                get;
                set;
            }
        }
    }
}

