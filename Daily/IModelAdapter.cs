﻿

namespace Daily
{
    public interface IModelAdapter<T>
    {
        int Count
        {
            get;
        }

        T Get(int i);
    }
}

