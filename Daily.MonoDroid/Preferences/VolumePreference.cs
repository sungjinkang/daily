
using Android.Preferences;
using Android.Util;
using Android.Content;
using Android.Widget;
using Android.Provider;
using Android.Media;
using System;
using Android.Views;
using Daily.MonoDroid;

namespace Daily.Preferences
{
    public class VolumePreference : DialogPreference
    {
        SeekBar alarmSeekBar;
        SeekBar notificationSeekBar;
        #region ctor
        public VolumePreference(Context context)
            : base(context)
        {
            Initialize();   
        }

        public VolumePreference(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();   

        }

        public VolumePreference(Context context, IAttributeSet attrs, int defStyleAttr)
            : base(context, attrs, defStyleAttr)
        {
            Initialize();   

        }
        public VolumePreference(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes)
            : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Initialize();   
        }

        void Initialize()
        {
            DialogLayoutResource = Resource.Layout.VolumePreference;
        }
        #endregion


        protected override void OnBindDialogView(View view)
        {
            base.OnBindDialogView(view);
            alarmSeekBar = view.FindViewById<SeekBar>(Resource.Id.AlarmSeekBar);
            notificationSeekBar = view.FindViewById<SeekBar>(Resource.Id.NotificationSeekBar);

            var audioManager = (AudioManager)Context.GetSystemService(Context.AudioService);
            alarmSeekBar.Max = audioManager.GetStreamMaxVolume(Stream.Alarm);
            alarmSeekBar.Progress = Settings.System.GetInt(Context.ContentResolver, Settings.System.VolumeAlarm);

            notificationSeekBar.Max = audioManager.GetStreamMaxVolume(Stream.Notification);
            notificationSeekBar.Progress = Settings.System.GetInt(Context.ContentResolver, Settings.System.VolumeNotification);
        }

        protected override void ShowDialog(Android.OS.Bundle state)
        {
            base.ShowDialog(state);
            Dialog.VolumeControlStream = Stream.Alarm;
            Dialog.KeyPress += OnDialogKeyPress;
        }

        void OnDialogKeyPress(object sender, DialogKeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down)
            {
                var progress = alarmSeekBar.Progress;
                switch (e.KeyCode)
                {
                    case Keycode.VolumeUp:
                        progress++;
                        if (progress > alarmSeekBar.Max)
                        {
                            progress = alarmSeekBar.Max;
                        }
                        break;
                    case Keycode.VolumeDown:
                        progress--;
                        if (progress < 0)
                        {
                            progress = 0;
                        }
                        break;
                    case Keycode.VolumeMute:
                        progress = 0;
                        break;
                }
                alarmSeekBar.Progress = progress;
            }
        }

        protected override void OnDialogClosed(bool positiveResult)
        {
            base.OnDialogClosed(positiveResult);
            if (positiveResult)
            {
                Settings.System.PutInt(Context.ContentResolver, Settings.System.VolumeNotification, notificationSeekBar.Progress);
                Settings.System.PutInt(Context.ContentResolver, Settings.System.VolumeAlarm, alarmSeekBar.Progress);
            }
        }
    }

}
