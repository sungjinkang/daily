﻿using Android.Content;
using Android.App;

namespace Daily.MonoDroid.Services
{
    [BroadcastReceiver]
    [IntentFilter(new []{ Intent.ActionBootCompleted })]
    public class SetToDosBroadcastReceiver : BroadcastReceiver
    {
        #region implemented abstract members of BroadcastReceiver

        public override void OnReceive(Context context, Intent intent)
        {
            new ConfigureAlarm(context, 
                IoC.Resolve<IStorageConnectionFactory>(), 
                IoC.Resolve<IPopupFactory>()).UpdateAlarms();
        }

        #endregion
    }
}

