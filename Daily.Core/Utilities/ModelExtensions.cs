﻿using System;
using Daily.Models;

namespace Daily.Core.Utilities
{
    public static class ModelExtensions
    {
        public static bool IsWeekly(this ToDo toDo)
        {
            var isWeekly = false;
            isWeekly = isWeekly || toDo.Sunday;
            isWeekly = isWeekly || toDo.Monday;
            isWeekly = isWeekly || toDo.Tuesday;
            isWeekly = isWeekly || toDo.Wednesday;
            isWeekly = isWeekly || toDo.Thursday;
            isWeekly = isWeekly || toDo.Friday;
            isWeekly = isWeekly || toDo.Saturday;
            return isWeekly;
        }

        public static bool DayOfWeekIsSet(this ToDo toDo, DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return toDo.Sunday;
                case DayOfWeek.Monday:
                    return toDo.Monday;
                case DayOfWeek.Tuesday:
                    return toDo.Tuesday;
                case DayOfWeek.Wednesday:
                    return toDo.Wednesday;
                case DayOfWeek.Thursday:
                    return toDo.Thursday;
                case DayOfWeek.Friday:
                    return toDo.Friday;
                case DayOfWeek.Saturday:
                    return toDo.Saturday;
                default:
                    return false;
            }
        }

        public static bool IntermittentFrom(this ToDo toDo, DateTime date)
        {
            if (date < toDo.NotifyDateTime)
            {
                return false;
            }
            var sum = toDo.DaysOn + toDo.DaysOff;
            var startDate = toDo.NotifyDateTime.Date;
            var diff = date.Date - startDate;
            var interval = diff.TotalDays % sum;
            return interval < toDo.DaysOn;
        }
    }
}

