﻿using System;
using Moq;
using NUnit.Framework;
using Daily.Models;
using Daily.Core.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Daily.Core.Adapters;

namespace Daily.Tests
{
    [TestFixture]
    public class ToDoViewModelTests : ViewModelTests
    {
        ToDo savedToDo;
        List<ToDo> toDos;
        List<DidDone> didDones;
        ToDoViewModel toDoViewModel;
        IAlert[] alerts;
        bool dialogShown;
        bool alarmsUpdated;
        bool alarmUnset;
         
        [SetUp]
        public void SetUp()
        {
            alarmsUpdated = false;
            alarmUnset = false;
            dialogShown = false;
            alerts = new []
            {
                new FakeAlert{ Id = 1, Path = "a" },
                new FakeAlert{ Id = 2, Path = "b" },
                new FakeAlert{ Id = 3, Path = "c" }
            };
            savedToDo = null;
            MockStorageConnectionFactory = new Mock<IStorageConnectionFactory>();
            toDos = new List<ToDo>
            { 
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
                    AlertId = 50,
                },
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                    AlertId = 100,
                }
            };
            didDones = new List<DidDone>
            {
                new DidDone() 
            };

            MockStorageConnection = new Mock<IStorageConnection>();
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(toDos.AsQueryable());
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());
            MockStorageConnection.Setup(s => s.Save<ToDo>(It.IsAny<ToDo>())).Callback<ToDo>(td => savedToDo = td);
            MockStorageConnectionFactory.Setup(s => s.Create()).Returns(MockStorageConnection.Object);

            MockAlertsAdapter = new Mock<IModelAdapter<IAlert>>();
            MockAlertsAdapter.Setup(aa => aa.Count).Returns(alerts.Length);
            MockAlertsAdapter.Setup(aa => aa.Get(It.IsAny<int>())).Returns<int>(i =>
                {
                    try
                    {
                        return alerts[i];
                    }
                    catch
                    {
                        return null;
                    }
                });

            MockPopupFactory = new Mock<IPopupFactory>();
            MockPopupFactory.Setup(pf => pf.Show(It.IsAny<string>())).Callback(() => dialogShown = true);

            MockAlarmService = new Mock<IConfigureAlarm>();
            MockAlarmService.Setup(a => a.UpdateAlarms()).Callback(() => alarmsUpdated = true);
            MockAlarmService.Setup(a => a.UnsetAlarm(It.IsAny<Guid>())).Callback(() => alarmUnset = true);
            toDoViewModel = new ToDoViewModel(MockStorageConnectionFactory.Object, 
                MockAlarmService.Object,
                MockPopupFactory.Object,
                new FrequencyAdapter(),
                new DayOfWeekAdapter(),
                MockAlertsAdapter.Object);

            MockPopupFactory.Setup(df => df.Show(It.IsAny<string>()))
                .Callback(() => dialogShown = true);



        }

        [TearDown]
        public void TearDown()
        {
            alerts = null;
            savedToDo = null;
            toDoViewModel = null;
        }

        [Test]
        public void WhenSetNotifyDateTime()
        {
            toDoViewModel.NotifyDateTime = new DateTime(1, 1, 1, 2, 2, 2);
            toDoViewModel.Save(new DateTime(1, 1, 1));
            Assert.AreEqual(toDoViewModel.NotifyDateTime, savedToDo.NotifyDateTime);
        }


        [Test]
        public void WhenSetTitle()
        {
            toDoViewModel.Title = "title";
            toDoViewModel.Save(new DateTime(1, 1, 1));
            Assert.AreEqual(toDoViewModel.Title, savedToDo.Title);
        }

        [Test]
        public void WhenSetDescription()
        {
            toDoViewModel.Description = "desc";
            toDoViewModel.Save(new DateTime(1, 1, 1));
            Assert.AreEqual(toDoViewModel.Description, savedToDo.Description);
        }

        [Test]
        public void WhenSetColor()
        {
            toDoViewModel.Color = "red";
            toDoViewModel.Save(new DateTime(1, 1, 1));
            Assert.AreEqual(toDoViewModel.Title, savedToDo.Title);
        }

        [Test]
        public void EmptyGuidGetsSetToGeneratedId()
        {
            toDoViewModel.Save(new DateTime(1, 1, 1));
            Assert.AreNotEqual(Guid.Empty, savedToDo.Id);
        }

        [Test]
        public void WhenSetFrequencyToDailyTest()
        {
            toDoViewModel.SelectedFrequencyIndex = (int)Frequency.Daily;
            toDoViewModel.Save(new DateTime(1, 1, 1));

            Assert.AreEqual(0, savedToDo.DaysOff);
            Assert.AreEqual(0, savedToDo.DaysOn);

            Assert.AreEqual(false, savedToDo.Sunday);
            Assert.AreEqual(false, savedToDo.Monday);
            Assert.AreEqual(false, savedToDo.Tuesday);
            Assert.AreEqual(false, savedToDo.Wednesday);
            Assert.AreEqual(false, savedToDo.Thursday);
            Assert.AreEqual(false, savedToDo.Friday);
            Assert.AreEqual(false, savedToDo.Saturday);
        }

        [Test]
        public void WhenSetFrequencyToWeeklyTest()
        {
            toDoViewModel.SelectedFrequencyIndex = (int)Frequency.Weekly;
            toDoViewModel.ToggleDayOfWeek = DayOfWeek.Monday;
            toDoViewModel.ToggleDayOfWeek = DayOfWeek.Saturday;

            toDoViewModel.ToggleDayOfWeek = DayOfWeek.Sunday;
            toDoViewModel.ToggleDayOfWeek = DayOfWeek.Sunday;
            toDoViewModel.Save(new DateTime(1, 1, 1));

            Assert.AreEqual(0, savedToDo.DaysOff);
            Assert.AreEqual(0, savedToDo.DaysOn);

            Assert.AreEqual(false, savedToDo.Sunday);
            Assert.AreEqual(true, savedToDo.Monday);
            Assert.AreEqual(false, savedToDo.Tuesday);
            Assert.AreEqual(false, savedToDo.Wednesday);
            Assert.AreEqual(false, savedToDo.Thursday);
            Assert.AreEqual(false, savedToDo.Friday);
            Assert.AreEqual(true, savedToDo.Saturday);
        }

        [Test]
        public void WhenSetFrequencyToIntermittent2On2OffTest()
        {
            toDoViewModel.SelectedFrequencyIndex = (int)Frequency.Intermittent;
            toDoViewModel.DaysOn = 3;
            toDoViewModel.DaysOff = 2;
            toDoViewModel.Save(new DateTime(1, 1, 1));

            Assert.AreEqual(3, savedToDo.DaysOn);
            Assert.AreEqual(2, savedToDo.DaysOff);

            Assert.AreEqual(false, savedToDo.Sunday);
            Assert.AreEqual(false, savedToDo.Monday);
            Assert.AreEqual(false, savedToDo.Tuesday);
            Assert.AreEqual(false, savedToDo.Wednesday);
            Assert.AreEqual(false, savedToDo.Thursday);
            Assert.AreEqual(false, savedToDo.Friday);
            Assert.AreEqual(false, savedToDo.Saturday);
        }

        [Test]
        public void WhenAlertSetToNegativeValue()
        {
            const int index = -1;
            toDoViewModel.SelectedAlertIndex = index;
            toDoViewModel.Save(new DateTime(1, 1, 1));
            var alert = MockAlertsAdapter.Object.Get(index) as FakeAlert;
            Assert.IsNull(alert);
        }

        [Test]
        public void WhenAlertSetToValidIndex()
        {
            const int index = 2;
            toDoViewModel.SelectedAlertIndex = index;
            toDoViewModel.Save(new DateTime(1, 1, 1));
            var alert = MockAlertsAdapter.Object.Get(index) as FakeAlert;
            Assert.AreEqual(101, savedToDo.AlertId);
            Assert.AreEqual(alert.Path, savedToDo.AlertSoundPath);
            Assert.IsTrue(alert.AlertSet);
        }

        [Test]
        public void WhenAlertSetToInvalidIndex()
        {
            const int index = int.MaxValue;
            toDoViewModel.SelectedAlertIndex = index;
            toDoViewModel.Save(new DateTime(1, 1, 1));
            var alert = MockAlertsAdapter.Object.Get(index) as FakeAlert;
            Assert.IsNull(alert);
        }

        [Test]
        public void WhenLoadToDoIdWhenTitleDescriptorsSet()
        {
            var guid = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(new List<ToDo>
                {
                    new ToDo
                    {
                        Id = guid,
                        Title = "t",
                        Description = "d",
                        Color = "c",
                    }
                }.AsQueryable());

            toDoViewModel.ToDoId = guid;

            Assert.AreEqual("t", toDoViewModel.Title);
            Assert.AreEqual("d", toDoViewModel.Description);
            Assert.AreEqual("c", toDoViewModel.Color);
        }

        [Test]
        public void WhenLoadToDoIdWhenIsWeekly()
        {
            var guid = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(new List<ToDo>
                {
                    new ToDo
                    {
                        Id = guid,
                        Tuesday = true,
                    }
                }.AsQueryable());

            toDoViewModel.ToDoId = guid;

            Assert.AreEqual(Frequency.Weekly, (Frequency)toDoViewModel.SelectedFrequencyIndex);
        }

        [Test]
        public void WhenLoadToDoIdWhenIsIntermittentDaysOnAndDaysOffBothGreater0()
        {
            var guid = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(new List<ToDo>
                {
                    new ToDo
                    {
                        Id = guid,
                        DaysOn = 1,
                        DaysOff = 1,
                    }
                }.AsQueryable());

            toDoViewModel.ToDoId = guid;

            Assert.AreEqual(Frequency.Intermittent, (Frequency)toDoViewModel.SelectedFrequencyIndex);
        }

        [Test]
        public void WhenDelete()
        {
            var guid = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            MockStorageConnection.Setup(s => s.Delete(It.IsAny<ToDo>())).Callback<ToDo>(td => toDos.Remove(td));

            toDoViewModel.ToDoId = guid;
            toDoViewModel.Delete();

            Assert.AreEqual(1, toDos.Count);
            Assert.AreNotEqual(guid, toDos[0].Id);
        }

        [Test]
        public void WhenDoToDo()
        {
            DidDone didDone = null;
            MockStorageConnection.Setup(s => s.Save<DidDone>(It.IsAny<DidDone>()))
            .Callback<DidDone>(dd =>
                {
                    didDone = dd;
                });
            var date = new DateTime(1, 1, 5);
            var fakeId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            toDoViewModel.ToDoId = fakeId;
            toDoViewModel.DoToDo(date);

            Assert.AreEqual(date, didDone.DoneDateTime);
            Assert.AreEqual(fakeId, didDone.ToDoId);
        }

        [Test]
        public void WhenUndoToDo()
        {
            var fakeId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            didDones = new List<DidDone>
            {
                new DidDone
                {
                    ToDoId = fakeId,
                    Id = new Guid(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    DoneDateTime = new DateTime(1, 1, 4)
                },
                new DidDone
                {
                    ToDoId = fakeId,
                    Id = new Guid(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    DoneDateTime = new DateTime(1, 1, 5)
                },
                new DidDone
                {
                    ToDoId = fakeId,
                    Id = new Guid(3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    DoneDateTime = new DateTime(1, 1, 6)
                },
            };
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());
            MockStorageConnection.Setup(s => s.Delete<DidDone>(It.IsAny<DidDone>())).Callback<DidDone>(dd => didDones.Remove(dd));
            var date = new DateTime(1, 1, 5);
            toDoViewModel.ToDoId = fakeId;
            toDoViewModel.UndoToDo(date);

            Assert.AreEqual(2, didDones.Count);
        }

        [Test]
        public void WhenSavedUpdatesAlarmService()
        {
            toDoViewModel.Save(new DateTime(1, 1, 1));

            Assert.IsTrue(alarmsUpdated);
        }

        [Test]
        public void WhenDoToDoUpdatesAlarmService()
        {
            toDoViewModel.ToDoId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            toDoViewModel.DoToDo(new DateTime(1, 1, 1));

            Assert.IsTrue(alarmUnset);
        }

        [Test]
        public void WhenDeleteUpdatesAlarmService()
        {
            toDoViewModel.ToDoId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            toDoViewModel.Delete();

            Assert.IsTrue(alarmUnset);
        }

        [Test]
        public void WhenSaveToDoClearDidDonesWithEarlierDates()
        {
            MockStorageConnection.Setup(sc => sc.Delete(It.IsAny<DidDone>())).Callback<DidDone>(dd => didDones.Remove(dd));
            toDos.Clear();
            var toDoId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3);
            toDos.Add(new ToDo
                {
                    Id = toDoId,
                    NotifyDateTime = new DateTime(2014, 4, 25),
                });
            didDones.Clear();
            didDones.Add(new DidDone
                {
                    DoneDateTime = new DateTime(2014, 4, 24),
                    Id = new Guid(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    ToDoId = toDoId,
                });
            didDones.Add(new DidDone
                {
                    DoneDateTime = new DateTime(2014, 4, 25),
                    Id = new Guid(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    ToDoId = toDoId,
                });
            didDones.Add(new DidDone
                {
                    DoneDateTime = new DateTime(2014, 4, 26),
                    Id = new Guid(3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                    ToDoId = toDoId,
                });
            toDoViewModel.ToDoId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3);

            toDoViewModel.Save(new DateTime(2014, 4, 25));

            Assert.AreEqual(2, didDones.Count);
            Assert.IsNull(didDones.FirstOrDefault(dd => dd.Id == new Guid(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));
            Assert.IsNotNull(didDones.FirstOrDefault(dd => dd.Id == new Guid(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));
            Assert.IsNotNull(didDones.FirstOrDefault(dd => dd.Id == new Guid(3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));
        }
    }
}

