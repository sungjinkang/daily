﻿var dailydidsControllers = angular.module('dailydidsControllers', []);

dailydidsControllers.controller('DayViewController', function ($scope, $routeParams, $http) {
    $http.get('GetDayViewModel', {
            params: {'date': $routeParams.date}
        }).success(function(data) {
            $scope.selectedDayViewModel = data;
    });
});

dailydidsControllers.controller('WeekViewController', function ($scope, $routeParams, $http) {
    $scope.selectedDayView = function(date) {
        var selectedDate = new Date($routeParams.date);
        date = new Date(date);
        if (selectedDate.getFullYear() == date.getFullYear() && 
            selectedDate.getMonth() == date.getMonth() && 
            selectedDate.getDay() == date.getDay()) {
            return "selected";
        }
    };

    $http.get('GetWeekViewModel', {
            params: {'date': $routeParams.date}
        }).success(function(data) {
            $scope.dayViewModels = data;
    });
});