﻿using System;

namespace Daily.Core.Utilities
{
    public static class DateTimeExtensions
    {
        public static DateTime LastSaturday = DateTime.MaxValue.AddDays(-(int)DateTime.MaxValue.DayOfWeek - 1);
        public static DateTime FirstSunday = DateTime.MinValue.AddDays((int)DayOfWeek.Saturday - (int)DateTime.MinValue.DayOfWeek + 1);
        public static int DaysCount = (LastSaturday - FirstSunday).Days;
        public static int WeeksCount = DaysCount / 7;



    }
}

