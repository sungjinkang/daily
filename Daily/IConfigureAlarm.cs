﻿using System;
using Daily.Models;

namespace Daily
{
    public interface IConfigureAlarm
    {
        void UpdateAlarms();
        void UnsetAlarm(Guid toDoId);
        void SnoozeAlarm(Guid toDoId, int minutes);
    }
}

