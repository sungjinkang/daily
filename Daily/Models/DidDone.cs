﻿using System;

namespace Daily.Models
{
    public class DidDone : ModelBase
    {
        public DidDone()
        {
        }

        public Guid ToDoId
        {
            get;
            set;
        }

        public DateTime DoneDateTime
        {
            get;
            set;
        }
    }
}

