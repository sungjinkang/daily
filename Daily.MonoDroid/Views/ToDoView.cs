﻿using System;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Daily.Core.ViewModels;
using System.ComponentModel;
using System.Threading.Tasks;
using Android.Graphics;
using Daily.Core;

namespace Daily.MonoDroid.Views
{
    public class ToDoView : ToDoViewBase, GestureDetector.IOnGestureListener
    {
        GestureDetector gestureDetector;

        TextView title;
        TextView description;
        TimeView timeView;
        ViewGroup layout;

        public event EventHandler<GuidEventArgs> Edit;
        public event EventHandler<GuidEventArgs> Changed;

        public event EventHandler DismissStart;
        public event EventHandler DismissEnd;

        public ToDoView(Context context)
            : base(context)
        {
            Initialize();
        }

        public ToDoView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public ToDoView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            gestureDetector = new GestureDetector(this);
            Inflate(Context, Resource.Layout.ToDoView, this);

            layout = FindViewById<ViewGroup>(Resource.Id.ToDoLayout);
            timeView = FindViewById<TimeView>(Resource.Id.TimeView);
            title = FindViewById<TextView>(Resource.Id.Title);
            description = FindViewById<TextView>(Resource.Id.Description);
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (e.ActionMasked == MotionEventActions.Up)
            {
                if (-ScrollX > Width / 3 * 2)
                {
                    DoToDo();
                }
                else
                {
                    CancelDismiss();
                }
            }
            else if (e.ActionMasked == MotionEventActions.Cancel)
            {
                CancelDismiss();
            }
            return gestureDetector.OnTouchEvent(e);
        }

        void DoToDo()
        {
            Visibility = ViewStates.Invisible;
            RaiseDismiss();
            Task.Run(() =>
                {
                    ViewModel.DoToDo(Date);
                    var changed = Changed;
                    if (changed != null)
                    {
                        changed(this, new GuidEventArgs(ViewModel.ToDoId));
                    }
                });
        }

        void CancelDismiss()
        {
            ScrollTo(0, 0);
            RaiseDismiss();
        }

        void RaiseDismiss()
        {
            var dismissEnd = DismissEnd;
            if (dismissEnd != null)
            {
                dismissEnd(this, EventArgs.Empty);
            }
        }

        public override void ClearListeners()
        {
            base.ClearListeners();
            Edit = null;
            DismissStart = null;
            DismissEnd = null;
            Changed = null;
        }

        protected override void OnViewModelChanged(ToDoViewModel value)
        {
            base.OnViewModelChanged(value);
            if (value != null)
            {
                title.Text = value.Title;
                description.Text = value.Description;
                timeView.Time = value.NotifyDateTime;

                var drawable = layout.Background;
                drawable.SetColorFilter(value.Color.ToColor(), PorterDuff.Mode.Multiply);
                layout.SetBackgroundDrawable(drawable);
            }
            UpdateVisibility();
        }

        void UpdateVisibility()
        {
            Visibility = ViewModel.DidDone ? ViewStates.Invisible : ViewStates.Visible;
        }

        protected override void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DidDone")
            {
                UpdateVisibility();
            }
        }

        public bool OnDown(MotionEvent e)
        {
            return true;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return false;
        }

        public void OnLongPress(MotionEvent e)
        {

        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            if (Math.Abs(distanceX) > Math.Abs(distanceY))
            {
                var dismissStart = DismissStart;
                if (dismissStart != null)
                {
                    dismissStart(this, EventArgs.Empty);
                }
            }
            var diff = (int)(ScrollX + distanceX);
            ScrollTo(diff, 0);

            return true;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            var editHandler = Edit;
            if (editHandler != null)
            {
                editHandler(this, new GuidEventArgs(ViewModel.ToDoId));
            }
            return true;
        }
    }

}

