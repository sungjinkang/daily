﻿using System.Collections.Generic;
using Daily.Core.ViewModels;

namespace Daily.Utilities
{
    public class ToDoViewModelEqualityComparer : IEqualityComparer<ToDoViewModel>
    {

        #region IEqualityComparer implementation

        public bool Equals(ToDoViewModel x, ToDoViewModel y)
        {
            return x.ToDoId == y.ToDoId;
        }

        public int GetHashCode(ToDoViewModel obj)
        {
            return obj.ToDoId.GetHashCode();
        }

        #endregion
    }
}

