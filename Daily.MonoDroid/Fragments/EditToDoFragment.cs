﻿using System;
using Android.OS;
using Android.Views;
using Daily.Core.ViewModels;
using Daily.Core.Adapters;
using Daily.MonoDroid.Views;
using Daily.MonoDroid.Activities;

namespace Daily.MonoDroid.Fragments
{
    public class EditToDoFragment : DailyFragmentBase
    {
        EditToDoView editToDoView;
        View save;
        View delete;
        View cancel;
        DateTime date;

        public void SetDate(DateTime date)
        {
            this.date = date;
            if (editToDoView != null)
            {
                Arguments.PutString(MainActivity.SelectedDateOfWeekKey, date.ToString());
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var supportActionBar = ((MainActivity)Activity).SupportActionBar;
            supportActionBar.SetCustomView(Resource.Layout.EditToDoActionBar);
            save = supportActionBar.CustomView.FindViewById<View>(Resource.Id.Save);
            delete = supportActionBar.CustomView.FindViewById<View>(Resource.Id.Delete);
            cancel = supportActionBar.CustomView.FindViewById<View>(Resource.Id.Cancel);

            var dayDateTime = DateTime.Parse(Arguments.GetString(MainActivity.SelectedDateOfWeekKey));
            Guid id;
            date = dayDateTime;
            try
            {
                id = new Guid(Arguments.GetString(MainActivity.ToDoIdKey));
            }
            catch
            {
                id = Guid.Empty;
            }
            editToDoView = new EditToDoView(Activity)
            {
                Id = 1,
                ViewModel = new ToDoViewModel(IoC.Resolve<IStorageConnectionFactory>(),
                    IoC.Resolve<IConfigureAlarm>(),
                    IoC.Resolve<IPopupFactory>(),
                    new FrequencyAdapter(),
                    new DayOfWeekAdapter(),
                    IoC.Resolve<IModelAdapter<IAlert>>())
                {
                    ToDoId = id,
                },
            };

            if (id == Guid.Empty)
            {
                delete.Visibility = ViewStates.Gone;
            }

            return editToDoView;
        }

        public override void OnResume()
        {
            base.OnResume();
            var supportActionBar = ((MainActivity)Activity).SupportActionBar;
            supportActionBar.SetIcon(null);
            supportActionBar.SetDisplayShowCustomEnabled(true);

            delete.Click += OnDeleteClick;
            save.Click += OnSaveClick;
            cancel.Click += OnCancelClick;
        }

        void OnDeleteClick(object sender, EventArgs e)
        {
            editToDoView.ViewModel.Delete();
            FragmentManager.PopBackStack();
        }

        void OnSaveClick(object sender, EventArgs e)
        {
            editToDoView.ViewModel.Save(date);
            FragmentManager.PopBackStack();
        }

        void OnCancelClick(object sender, EventArgs e)
        {
            FragmentManager.PopBackStack();
        }
        
        public override void OnPause()
        {
            base.OnPause();
            delete.Click -= OnDeleteClick;
            save.Click -= OnSaveClick;
            cancel.Click -= OnCancelClick;
            var supportActionBar = ((MainActivity)Activity).SupportActionBar;
            supportActionBar.SetIcon(Resource.Drawable.PaddedIcon);
            supportActionBar.SetDisplayShowCustomEnabled(false);
        }

    }
}

