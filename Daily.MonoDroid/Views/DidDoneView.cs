﻿using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Daily.Core.ViewModels;

namespace Daily.MonoDroid.Views
{
    public class DidDoneView : ToDoViewBase
    {
        TextView title;

        public DidDoneView(Context context)
            : base(context)
        {
            Initialize();
        }

        public DidDoneView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public DidDoneView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            var view = Inflate(Context, Resource.Layout.DidDoneView, this);
            title = view.FindViewById<TextView>(Resource.Id.Title);
        }

        protected override void OnViewModelChanged(ToDoViewModel value)
        {
            if (value != null)
            {
                title.Text = string.IsNullOrEmpty(value.Title) ? "Done" : value.Title;
            }
        }

        protected override void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

    }
}

