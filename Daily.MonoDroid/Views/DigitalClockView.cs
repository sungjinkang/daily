﻿
using System;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Daily.MonoDroid.Views
{
    public class DigitalClockView : ViewGroup
    {
        TextView colon;
        public TextView Hour
        {
            get;
            private set;
        }
        public TextView Minute
        {
            get;
            private set;
        }
        public TextView TimeOfDay
        {
            get;
            private set;
        }

        public DigitalClockView(Context context)
            : base(context)
        {
            Initialize();
        }

        public DigitalClockView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public DigitalClockView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        DateTime time;
        public DateTime Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
                Hour.Text = value.ToString("hh");
                Minute.Text = value.ToString("mm");
                TimeOfDay.Text = value.ToString("tt");
            }
        }

        void Initialize()
        {
            Hour = new TextView(Context)
            {
                Gravity = GravityFlags.Center,
            };
            AddView(Hour);
            colon = new TextView(Context)
            {
                Text = ":",
                Gravity = GravityFlags.Center,
            };
            AddView(colon);
            Minute = new TextView(Context)
            {
                Gravity = GravityFlags.Center,
            };
            AddView(Minute);
            TimeOfDay = new TextView(Context)
            { 
                Gravity = GravityFlags.Center,
            };
            AddView(TimeOfDay);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            var width = 200;
            var widthMode = MeasureSpec.GetMode(widthMeasureSpec);
            var widthSize = MeasureSpec.GetSize(widthMeasureSpec);

            if (widthMode == MeasureSpecMode.AtMost)
            {
                width = Math.Min(width, widthSize);
            }
            else if (widthMode == MeasureSpecMode.Exactly)
            {
                width = widthSize;
            }

            var heightMode = MeasureSpec.GetMode(heightMeasureSpec);
            var heightSize = MeasureSpec.GetSize(heightMeasureSpec);

            var height = width / 4;
            if (heightMode == MeasureSpecMode.AtMost)
            {
                height = Math.Min(height, heightSize);
            }
            else if (heightMode == MeasureSpecMode.Exactly)
            {
                height = heightSize;
            }

            SetMeasuredDimension(width, height);

            var fontHeight = height * 3 / 4;

            colon.Measure(MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified),
                MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly));
            colon.SetTextSize(ComplexUnitType.Px, fontHeight);

            var numbersWidth = (width - colon.MeasuredWidth) / 3;
            Hour.Measure(MeasureSpec.MakeMeasureSpec(numbersWidth, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly));
            Hour.SetTextSize(ComplexUnitType.Px, fontHeight);

            Minute.Measure(MeasureSpec.MakeMeasureSpec(numbersWidth, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly));
            Minute.SetTextSize(ComplexUnitType.Px, fontHeight);

            TimeOfDay.Measure(MeasureSpec.MakeMeasureSpec(numbersWidth, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly));
            TimeOfDay.SetTextSize(ComplexUnitType.Px, fontHeight);
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            var height = Height;
            var xOffset = l;

            Hour.Layout(
                xOffset,
                t,
                xOffset + Hour.MeasuredWidth,
                t + height);
            xOffset = Hour.MeasuredWidth;


            colon.Layout(
                xOffset,
                t,
                xOffset + colon.MeasuredWidth,
                t + height);
            xOffset += colon.MeasuredWidth;


            Minute.Layout(
                xOffset,
                t,
                xOffset + Minute.MeasuredWidth,
                t + height);

            xOffset += Minute.MeasuredWidth;

            TimeOfDay.Layout(
                xOffset,
                t,
                xOffset + TimeOfDay.MeasuredWidth,
                t + height);
        }
    }
}

