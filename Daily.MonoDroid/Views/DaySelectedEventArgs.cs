using System;

namespace Daily.MonoDroid.Views
{
    
    public class DaySelectedEventArgs : EventArgs
    {
        public DateTime DateTime
        {
            get;
            set;
        }
    }
    
}
