﻿using Android.OS;
using Android.Preferences;

namespace Daily.MonoDroid.Fragments
{
    public class SettingsFragment : PreferenceFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            AddPreferencesFromResource(Resource.Layout.Settings);
        }
    }
}

