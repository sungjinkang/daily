﻿using System;
using Daily.Models;
using System.Linq;

namespace Daily.Core.ViewModels
{
    public class ToDoViewModel : ViewModel
    {
        readonly IStorageConnectionFactory storageConnectionFactory;
        readonly IConfigureAlarm configureAlarm;
        readonly IModelAdapter<IAlert> alertsAdapter;
        readonly IModelAdapter<IMediator<bool, DayOfWeek>> dayOfWeekAdapter;
        readonly IModelAdapter<Frequency> frequencyAdapter;
        readonly IPopupFactory popupFactory;
        Guid toDoId;


        public ToDoViewModel(IStorageConnectionFactory storageConnectionFactory,
            IConfigureAlarm configureAlarm,
            IPopupFactory popupFactory, 
            IModelAdapter<Frequency> frequencyAdapter,
            IModelAdapter<IMediator<bool, DayOfWeek>> dayOfWeekAdapter,
            IModelAdapter<IAlert> alertsAdapter)
        {
            this.frequencyAdapter = frequencyAdapter;
            this.dayOfWeekAdapter = dayOfWeekAdapter;
            this.alertsAdapter = alertsAdapter;
            this.storageConnectionFactory = storageConnectionFactory;
            this.configureAlarm = configureAlarm;
            this.popupFactory = popupFactory;
        }

        public IModelAdapter<IMediator<bool, DayOfWeek>> DayOfWeekAdapter
        {
            get
            {
                return dayOfWeekAdapter;
            }
        }

        public IModelAdapter<IAlert> AlertsAdapter
        {
            get
            {
                return alertsAdapter;
            }
        }

        public IModelAdapter<Frequency> FrequencyAdapter
        {
            get
            {
                return frequencyAdapter;
            }
        }

        public Guid ToDoId
        {
            get
            {
                return toDoId;
            }
            set
            {
                toDoId = value;
                if (toDoId != Guid.Empty && storageConnectionFactory != null)
                {
                    using (var storage = storageConnectionFactory.Create())
                    {
                        var toDo = storage.Fetch<ToDo>().FirstOrDefault(td => td.Id == toDoId);
                        Title = toDo.Title;
                        Description = toDo.Description;
                        Color = toDo.Color;

                        NotifyDateTime = toDo.NotifyDateTime;

                        if (frequencyAdapter != null)
                        {
                            if (dayOfWeekAdapter != null && (toDo.Sunday ||
                                toDo.Monday ||
                                toDo.Tuesday ||
                                toDo.Wednesday ||
                                toDo.Thursday ||
                                toDo.Friday ||
                                toDo.Saturday))
                            {
                                SelectedFrequencyIndex = (int)Frequency.Weekly;
                                var count = dayOfWeekAdapter.Count;
                                for (int i = 0; i < count; i++)
                                {
                                    var mediator = dayOfWeekAdapter.Get(i);
                                    switch (mediator.Item)
                                    {
                                        case DayOfWeek.Sunday:
                                            mediator.Value = toDo.Sunday;
                                            break;
                                        case DayOfWeek.Monday:
                                            mediator.Value = toDo.Monday;
                                            break;
                                        case DayOfWeek.Tuesday:
                                            mediator.Value = toDo.Tuesday;
                                            break;
                                        case DayOfWeek.Wednesday:
                                            mediator.Value = toDo.Wednesday;
                                            break;
                                        case DayOfWeek.Thursday:
                                            mediator.Value = toDo.Thursday;
                                            break;
                                        case DayOfWeek.Friday:
                                            mediator.Value = toDo.Friday;
                                            break;
                                        case DayOfWeek.Saturday:
                                            mediator.Value = toDo.Saturday;
                                            break;
                                    }
                                }
                            }
                            else if (frequencyAdapter != null && toDo.DaysOn > 0 && toDo.DaysOff > 0)
                            {
                                SelectedFrequencyIndex = (int)Frequency.Intermittent;
                                DaysOn = toDo.DaysOn;
                                DaysOff = toDo.DaysOff;
                            }
                            else
                            {
                                SelectedFrequencyIndex = (int)Frequency.Daily;
                            }
                        }

                        if (alertsAdapter != null && !string.IsNullOrEmpty(toDo.AlertSoundPath))
                        {
                            var count = alertsAdapter.Count;
                            for (int i = 0; i < count; i++)
                            {
                                if (alertsAdapter.Get(i).Path == toDo.AlertSoundPath)
                                {
                                    SelectedAlertIndex = i;
                                    break;
                                }
                            }
                        }
                        alertSoundPath = toDo.AlertSoundPath;
                        alertId = toDo.AlertId;
                    }
                }
            }
        }

        string alertSoundPath;
        public string AlertSoundPath
        {
            get
            {
                return alertSoundPath;
            }
        }

        int alertId;
        public int AlertId
        {
            get
            {
                return alertId;
            }
        }

        DateTime notifyDateTime;
        public DateTime NotifyDateTime
        {
            get
            {
                return notifyDateTime;
            }
            set
            {
                notifyDateTime = value;
                InvokePropertyChanged();
            }
        }

        string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                InvokePropertyChanged();
            }
        }

        string color;
        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                InvokePropertyChanged();
            }
        }

        int selectedFrequencyIndex;
        public int SelectedFrequencyIndex
        {
            get
            {
                return selectedFrequencyIndex;
            }
            set
            {
                selectedFrequencyIndex = value;
                InvokePropertyChanged();
            }
        }

        int selectedAlertIndex;
        public int SelectedAlertIndex
        {
            get
            {
                return selectedAlertIndex;
            }
            set
            {
                selectedAlertIndex = value;
                InvokePropertyChanged();
            }
        }

        public DayOfWeek ToggleDayOfWeek
        {
            set
            {
                var mediator = dayOfWeekAdapter.Get((int)value);
                mediator.Value = !mediator.Value;
            }
        }

        int daysOn;
        public int DaysOn
        {
            get
            {
                return daysOn;
            }
            set
            {
                daysOn = value;
                InvokePropertyChanged();
            }
        }

        int daysOff;
        public int DaysOff
        {
            get
            {
                return daysOff;
            }
            set
            {
                daysOff = value;
                InvokePropertyChanged();
            }
        }

        bool didDone;
        public bool DidDone
        {
            get
            {
                return didDone;
            }
            set
            {
                didDone = value;
                InvokePropertyChanged();
            }
        }


        public void Save(DateTime date)
        {
            date = date.ToUniversalTime();
            using (var storage = storageConnectionFactory.Create())
            {
                int alertId;
                if (toDoId == Guid.Empty)
                {
                    toDoId = Guid.NewGuid();
                    try
                    {
                        alertId = storage.Fetch<ToDo>().Max(td => td.AlertId) + 1;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        alertId = 1;
                    }
                }
                else
                {
                    alertId = storage.Fetch<ToDo>().First(td => td.Id == toDoId).AlertId;
                }
                IAlert selectedAlert;
                var toDo = new ToDo
                {
                    NotifyDateTime = date.Date.Add(NotifyDateTime.TimeOfDay),
                    Id = toDoId,
                    Title = Title,
                    Description = Description,
                    Color = Color,
                    AlertId = alertId,
                };
                if (alertsAdapter != null && selectedAlertIndex >= 0 && selectedAlertIndex < alertsAdapter.Count)
                {
                    selectedAlert = alertsAdapter.Get(selectedAlertIndex);
                    toDo.AlertSoundPath = selectedAlert.Path;

                    selectedAlert.SetAlert();
                }
                if (dayOfWeekAdapter != null &&
                    SelectedFrequencyIndex == (int)Frequency.Weekly)
                {
                    var count = dayOfWeekAdapter.Count;
                    for (int i = 0; i < count; i++)
                    {
                        var mediator = dayOfWeekAdapter.Get(i);
                        switch (mediator.Item)
                        {
                            case DayOfWeek.Sunday:
                                toDo.Sunday = mediator.Value;
                                break;
                            case DayOfWeek.Monday:
                                toDo.Monday = mediator.Value;
                                break;
                            case DayOfWeek.Tuesday:
                                toDo.Tuesday = mediator.Value;
                                break;
                            case DayOfWeek.Wednesday:
                                toDo.Wednesday = mediator.Value;
                                break;
                            case DayOfWeek.Thursday:
                                toDo.Thursday = mediator.Value;
                                break;
                            case DayOfWeek.Friday:
                                toDo.Friday = mediator.Value;
                                break;
                            case DayOfWeek.Saturday:
                                toDo.Saturday = mediator.Value;
                                break;
                        }
                    }
                }
                if (SelectedFrequencyIndex == (int)Frequency.Intermittent)
                {
                    toDo.DaysOn = DaysOn;
                    toDo.DaysOff = DaysOff;
                }
                try
                {
                    storage.Save(toDo);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    popupFactory.Show("Failed to Save");
                    return;
                }
                configureAlarm.UpdateAlarms();

                var earlierDidDones = storage.Fetch<DidDone>().Where(dd => dd.ToDoId == ToDoId && dd.DoneDateTime.Date < date.Date).ToList();
                foreach (var dd in earlierDidDones)
                {
                    storage.Delete(dd);
                }
            }
        }

        public void Delete()
        {
            using (var storage = storageConnectionFactory.Create())
            {
                var toDo = storage.Fetch<ToDo>().First(td => td.Id == ToDoId);
                storage.Delete(toDo);
                configureAlarm.UnsetAlarm(toDo.Id);
            }
        }

        public void DoToDo(DateTime date)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("date is " + date + " id is " + toDoId);
                using (var storage = storageConnectionFactory.Create())
                {
                    storage.Save<DidDone>(new DidDone
                        {
                            DoneDateTime = date,
                            Id = Guid.NewGuid(),
                            ToDoId = ToDoId,
                        });

                    var toDo = storage.Fetch<ToDo>().First(td => td.Id == ToDoId);
                    configureAlarm.UnsetAlarm(toDo.Id);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                popupFactory.Show("An error occurred while attempting to save");
            }
        }

        public void UndoToDo(DateTime date)
        {
            try
            {
                using (var storage = storageConnectionFactory.Create())
                {
                    var didDones = storage.Fetch<DidDone>()
                        .Where(dd => dd.ToDoId == toDoId && dd.DoneDateTime.Date == date.Date)
                        .ToList();
                    foreach (var dd in didDones)
                    {
                        storage.Delete<DidDone>(dd);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                popupFactory.Show("An error occurred while attempting to undo");
            }
        }
    }

}