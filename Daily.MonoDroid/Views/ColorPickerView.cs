﻿using Android.Content;
using Android.Util;
using Android.Views;
using System.Collections.Generic;
using Android.Graphics;
using System;

namespace Daily.MonoDroid.Views
{
    public class ColorPickerView : ViewGroup, GestureDetector.IOnGestureListener
    {
        public static Color Red = new Color(255, 50, 50);
        public static Color Orange = new Color(255, 176, 0);
        public static Color Yellow = new Color(255, 252, 183);
        public static Color Green = new Color(150, 251, 130);
        public static Color Blue = new Color(130, 167, 251);
        public static Color Indigo = new Color(130, 136, 251);
        public static Color Violet = new Color(167, 130, 251);

        readonly Dictionary<Color, View> colorToViewLookUp = new Dictionary<Color, View>();
        readonly List<Color> colors = new List<Color>
        {
            Color.White,
            Red,
            Orange,
            Yellow,
            Green,
            Blue,
            Indigo,
            Violet
        };

        GestureDetector gestureDetector;

        public class ColorChangeEventArgs : EventArgs
        {
            public Color Color
            {
                get;
                set;
            }
        }

        public event EventHandler<ColorChangeEventArgs> ColorChanged;
        public event EventHandler ChangeColorStart;
        public event EventHandler ChangeColorEnd;

        public ColorPickerView(Context context)
            : base(context)
        {
            Initialize();
        }

        public ColorPickerView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        void Initialize()
        {
            
            foreach (var color in colors)
            {
                var view = new View(Context);
                view.SetBackgroundColor(color);
                colorToViewLookUp[color] = view;
                AddView(view);
            }

            gestureDetector = new GestureDetector(this);
        }

        View current;
        View neighbor;
        Color color;
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;

                if (!colorToViewLookUp.ContainsKey(value))
                {
                    var view = new View(Context);
                    view.SetBackgroundColor(value);
                    colorToViewLookUp[value] = view;
                    colors.Add(value);
                    AddView(view);
                }
                current = colorToViewLookUp[value];

                var colorChanged = ColorChanged;
                if (colorChanged != null)
                {
                    colorChanged(this, new ColorChangeEventArgs{ Color = value });
                }

                RequestLayout();
            }
        }


        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            var width = Width;
            var height = Height;
            var currentWidth = width / 2;
            var colorWidth = currentWidth / (colorToViewLookUp.Count - 1);
            var xOffset = l;
            foreach (var c in colors)
            {
                var view = colorToViewLookUp[c];
                if (view == current)
                {
                    view.Measure(currentWidth, height);
                    view.Layout(
                        xOffset,
                        t,
                        xOffset + currentWidth + adjustCurrentWidth,
                        t + height
                    );
                }
                else if (view == neighbor)
                {
                    view.Measure(colorWidth, height);
                    view.Layout(
                        xOffset,
                        t,
                        xOffset + colorWidth + adjustNeighborWidth,
                        t + height
                    );
                    if (view.Width >= currentWidth)
                    {
                        ClearNeighbor();
                        Color = c;
                        return;
                    }
                }
                else
                {
                    view.Measure(colorWidth, height);
                    view.Layout(
                        xOffset,
                        t,
                        xOffset + colorWidth,
                        t + height
                    );

                }
                xOffset += view.Width;

            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (e.ActionMasked == MotionEventActions.Up ||
                e.ActionMasked == MotionEventActions.Cancel)
            {
                ClearNeighbor();
                RequestLayout();
            }
            return gestureDetector.OnTouchEvent(e);
        }

        void ClearNeighbor()
        {
            adjustCurrentWidth = 0;
            adjustNeighborWidth = 0;
            neighbor = null;
            var handler = ChangeColorEnd;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public bool OnDown(MotionEvent e)
        {
            return true;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return true;
        }

        public void OnLongPress(MotionEvent e)
        {
        }
        int adjustCurrentWidth;
        int adjustNeighborWidth;
        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            var handler = ChangeColorStart;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
            var position = colors.IndexOf(color);
            if (distanceX < 0)
            {
                if (neighbor == null && position > 0)
                {             
                    neighbor = colorToViewLookUp[colors[position - 1]];
                }

                if (neighbor != null)
                {
                    adjustCurrentWidth += (int)distanceX;
                    adjustNeighborWidth -= (int)distanceX;

                }
            }
            else if (distanceX > 0)
            {
                if (neighbor == null && position < colors.Count - 1)
                {             
                    neighbor = colorToViewLookUp[colors[position + 1]];
                }

                if (neighbor != null)
                {
                    adjustCurrentWidth -= (int)distanceX;
                    adjustNeighborWidth += (int)distanceX;
                }
            }

            RequestLayout();
            return true;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            var midPoint = Width / 2;
            if (e.GetX() > midPoint)
            {
                var next = colors.IndexOf(color) + 1;
                if (next >= colors.Count)
                {
                    next = colors.Count - 1;
                }
                Color = colors[next];
            }
            else if (e.GetX() < midPoint)
            {
                var prev = colors.IndexOf(color) - 1;
                if (prev < 0)
                {
                    prev = 0;
                }
                Color = colors[prev];
            }
            return true;
        }
    }
}

