﻿

using Android.App;
using Android.Runtime;
using System;
using Daily.MonoDroid.Adapters;
using Daily.MonoDroid.Services;

namespace Daily.MonoDroid
{
    [Application(Theme = "@style/DailyTheme")]
    public class App : Application
    {
        public App()
        {
        }

        public App(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            var storageConnectionFactory = new StorageConnectionFactory();
            var popupFactory = new PopupFactory();
            IoC.Register<IStorageConnectionFactory>(new Lazy<object>(() => storageConnectionFactory));
            IoC.Register<IPopupFactory>(new Lazy<object>(() => popupFactory));
            IoC.Register<IModelAdapter<IAlert>>(new Lazy<object>(() => new AlertsAdapter(Context)));
            IoC.Register<IConfigureAlarm>(new Lazy<object>(() => new ConfigureAlarm(Context, storageConnectionFactory, popupFactory)));
        }
    }
}

