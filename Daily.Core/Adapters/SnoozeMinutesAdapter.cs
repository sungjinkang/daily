using System;
using System.Collections.Generic;
using System.Linq;


namespace Daily.Core.Adapters
{

    public class SnoozeMinutesAdapter : IModelAdapter<int>
    {
        readonly List<int> minutesRange;
        public SnoozeMinutesAdapter()
        {
            minutesRange = Enumerable.Range(1, 30).ToList();
        }
        #region IModelAdapter implementation
        
        public int Get(int i)
        {
            return minutesRange[i];
        }
        
        public int Count
        {
            get
            {
                return minutesRange.Count;
            }
        }
        
        #endregion
        
    }
}
