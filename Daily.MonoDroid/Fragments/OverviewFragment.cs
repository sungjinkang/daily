﻿using System;

using Android.OS;
using Android.Views;
using Daily.Core.ViewModels;
using Android.App;
using Android.Content;
using Daily.MonoDroid.Views;
using Daily.MonoDroid.Activities;

namespace Daily.MonoDroid.Fragments
{
    public class OverviewFragment : DailyFragmentBase
    {
        DayView dayView;
        DateTime selectedDateOfWeek = DateTime.Today;
        View addButton;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            HasOptionsMenu = true;
            var view = inflater.Inflate(Resource.Layout.Overview, null);

            dayView = view.FindViewById<DayView>(Resource.Id.DayView);
            dayView.ViewModel = new DayViewModel(IoC.Resolve<IStorageConnectionFactory>(), IoC.Resolve<IPopupFactory>(), IoC.Resolve<IConfigureAlarm>());
            addButton = view.FindViewById<View>(Resource.Id.AddButton);
            if (savedInstanceState != null)
            {
                var selectedDateOfWeekString = savedInstanceState.GetString(MainActivity.SelectedDateOfWeekKey);
                if (selectedDateOfWeekString != null)
                {
                    selectedDateOfWeek = DateTime.Parse(selectedDateOfWeekString);
                }
            }
            addButton.Click += delegate
            {
                ShowEditToDoFragment(null);
            };
            return view;
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutString(MainActivity.SelectedDateOfWeekKey, selectedDateOfWeek.ToString());
        }

        public override void OnResume()
        {
            base.OnResume();
            Reload();
            ReloadActivity();
            ((MainActivity)Activity).SetWeekViewEnabled(true);
            dayView.Edit += OnDayViewEdit;
            dayView.Changed += OnDayViewChanged;

            var alarmManager = Activity.GetSystemService(Context.AlarmService) as AlarmManager;
            if (alarmManager != null)
            {
//                var intent = new Intent(Activity, typeof(AlertToDo.SetToDosBroadcastReceiver));
//                var pendingItent = PendingIntent.GetBroadcast(Activity, 0, intent, PendingIntentFlags.UpdateCurrent);
//                
//                alarmManager.SetRepeating(AlarmType.RtcWakeup, 
//                    SystemClock.ElapsedRealtime(),
//                    AlarmManager.IntervalDay,
//                    pendingItent);
            }
        }

        void ReloadActivity()
        {
            var activity = Activity as MainActivity;
            if (activity != null)
            {
                activity.Reload();
            }
        }

        void OnDayViewChanged(object sender, EventArgs e)
        {
            ReloadActivity();
        }

        public override void OnPause()
        {
            base.OnPause();
            dayView.Edit -= OnDayViewEdit;
            dayView.Changed -= OnDayViewChanged;
        }

        public void Reload()
        {
            if (dayView != null)
            {
                dayView.SetDate(selectedDateOfWeek);
            }
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);
            inflater.Inflate(Resource.Menu.MainMenu, menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Resource.Id.SettingsAction)
            {
                StartActivity(new Intent(Activity, typeof(SettingsActivity)));
            }
            return base.OnOptionsItemSelected(item);
        }

        public void ShowEditToDoFragment(string toDoId)
        {
            var bundle = new Bundle();
            bundle.PutString(MainActivity.SelectedDateOfWeekKey, selectedDateOfWeek.ToString());
            if (!string.IsNullOrEmpty(toDoId))
            {
                bundle.PutString(MainActivity.ToDoIdKey, toDoId);
                ((MainActivity)Activity).SetWeekViewEnabled(false);
            }

            var editToDoFragment = new EditToDoFragment
            {
                Arguments = bundle,
            };
            FragmentManager.BeginTransaction()
                .Replace(Resource.Id.MainLayout, editToDoFragment, typeof(EditToDoFragment).Name)
                .AddToBackStack(typeof(EditToDoFragment).Name)
                .Commit();
        }

        void OnDayViewEdit(object sender, EventArgs e)
        {
            var view = sender as ToDoViewBase;
            if (view != null)
            {
                ShowEditToDoFragment(view.ViewModel.ToDoId.ToString());
            }
        }

        public DateTime SelectedDateOfWeek
        {
            get
            {
                return selectedDateOfWeek;
            }
            set
            {
                selectedDateOfWeek = value;
                Reload();
            }
        }

    }
}

