﻿
using System;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Daily.Core.ViewModels;
using Daily.Core.Utilities;
using System.Collections.Generic;

namespace Daily.MonoDroid.Views
{
    public class WeekViewVertical : ListView
    {
        public event EventHandler<DaySelectedEventArgs> DaySelected;
        readonly List<DayViewSmall> checkedDayViews = new List<DayViewSmall>();
        public WeekViewVertical(Context context)
            : base(context)
        {
            Initialize();
        }

        public WeekViewVertical(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public WeekViewVertical(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            VerticalScrollBarEnabled = false;
            Divider = null;
            SetSelector(Resource.Drawable.DayViewSelector);
            ItemClick += (sender, e) =>
            {
                if (!Enabled)
                {
                    return;
                }
                var dayView = e.View as DayViewSmall;
                if (dayView != null)
                {
                    foreach (var v in checkedDayViews)
                    {
                        v.Checked = false;
                    }
                    dayView.Checked = true;
                    checkedDayViews.Add(dayView);
                    var daySelected = DaySelected;
                    if (daySelected != null)
                    {  
                        daySelected(e.View, new DaySelectedEventArgs{ DateTime = dayView.ViewModel.Date });
                    }
                }

            };
            Adapter = new WeekViewListAdapter(this);
        }

        public new bool Enabled
        {
            get;
            set;
        }

        DateTime date;
        public void SetDate(DateTime date)
        {
            this.date = date;
            var day = (int)date.DayOfWeek;
            var sunday = date.AddDays(-(int)date.DayOfWeek);
            var weeks = ((sunday - DateTimeExtensions.FirstSunday).Days / 7);

            var pos = day + weeks * 7;
            SetSelectionFromTop(pos, 100);
        }

        public void NotifyDataSetChanged()
        {
            var adapter = Adapter as WeekViewListAdapter;
            if (adapter != null)
            {
                adapter.NotifyDataSetChanged();
            }
        }

        class WeekViewListAdapter : BaseAdapter<DayViewModel>
        {
            readonly WeekViewVertical weekView;
            public WeekViewListAdapter(WeekViewVertical verticalWeekView)
            {
                weekView = verticalWeekView;
            }

            public override long GetItemId(int position)
            {
                return position;
            }
            public override View GetView(int position, View convertView, ViewGroup parent)
            {
                var dayView = convertView as DayViewSmall ?? new DayViewSmall(parent.Context)
                {
                    ViewModel = new DayViewModel(IoC.Resolve<IStorageConnectionFactory>(), IoC.Resolve<IPopupFactory>(), null),
                };

                var index = position % 7;
                var section = position / 7;
                var date = DateTimeExtensions.FirstSunday.AddDays((section * 7) + index);

                dayView.SetDate(date);
                dayView.Checked = weekView.date == dayView.ViewModel.Date;
                return dayView;
            }

            public override int Count
            {
                get
                {
                    return DateTimeExtensions.WeeksCount * 7;
                }
            }

            public override DayViewModel this[int index]
            {
                get
                {
                    return null;
                }
            }
        }
    }
}

