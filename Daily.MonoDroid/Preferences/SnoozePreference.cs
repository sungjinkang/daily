﻿using Android.Content;
using Android.Util;
using Android.Preferences;
using Android.Widget;
using Android.Views;
using System;
using Daily.MonoDroid.Adapters;
using System.Linq;
using Daily.Core.Adapters;
using Daily.MonoDroid;

namespace Daily.Preferences
{
    public class SnoozePreference : DialogPreference
    {
        ListView minutesList;
        #region ctor
        public SnoozePreference(Context context)
            : base(context)
        {
            Initialize();
        }

        public SnoozePreference(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public SnoozePreference(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            DialogLayoutResource = Resource.Layout.SnoozePreference;
        }
        #endregion

        protected override void OnPrepareDialogBuilder(Android.App.AlertDialog.Builder builder)
        {
            base.OnPrepareDialogBuilder(builder);
            builder.SetNegativeButton((string)null, (EventHandler<DialogClickEventArgs>)null);
            builder.SetPositiveButton((string)null, (EventHandler<DialogClickEventArgs>)null);
        }
        protected override void OnBindDialogView(View view)
        {
            base.OnBindDialogView(view);
            minutesList = view.FindViewById<ListView>(Resource.Id.MinutesList);

            var adapter = new RadioListAdapter<int>(new SnoozeMinutesAdapter());
            minutesList.Adapter = adapter;
            var pos = GetPersistedInt(10) - 1;
            adapter.CurrentPosition = pos;
            minutesList.ItemClick += OnItemClicked;
            minutesList.SetSelection(pos);
        }

        void OnItemClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            PersistInt(e.Position + 1);
            Dialog.Dismiss();
        }

        protected override void OnDialogClosed(bool positiveResult)
        {
            base.OnDialogClosed(positiveResult);
            minutesList.ItemClick -= OnItemClicked;
            if (positiveResult)
            {
                PersistInt((int)minutesList.SelectedItem);
            }
        }

    }

}

