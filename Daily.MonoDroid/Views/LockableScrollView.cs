﻿using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Daily.MonoDroid.Views
{
    public class LockableScrollView : ScrollView
    {
        public LockableScrollView(Context context)
            : base(context)
        {
            Initialize();
        }

        public LockableScrollView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public LockableScrollView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
        }

        public bool LockScroll
        {
            get;
            set;
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (LockScroll)
            {
                return false;
            }
            return base.OnTouchEvent(e);
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev)
        {
            if (LockScroll)
            {
                return false;
            }
            return base.OnInterceptTouchEvent(ev);
        }
    }
}

