﻿using System;
using Daily.Models;

namespace Daily.Web.Services
{
    public class ConfigureAlarm : IConfigureAlarm
    {
        public ConfigureAlarm(Guid userId)
        {
        }

        public void UpdateAlarms()
        {

        }

        public void UnsetAlarm(Guid toDoId)
        {
        }

        public void SnoozeAlarm(Guid toDoId, int minutes)
        {
        }
    }
}

