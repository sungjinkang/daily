using Moq;
using System;

namespace Daily.Tests
{
    public class ViewModelTests
    {
        protected Mock<IStorageConnectionFactory> MockStorageConnectionFactory;
        protected Mock<IStorageConnection> MockStorageConnection;
        protected Mock<IPopupFactory> MockPopupFactory;
        protected Mock<IModelAdapter<IAlert>> MockAlertsAdapter;
        protected Mock<IModelAdapter<DayOfWeek>> MockDayOfWeekAdapter;
        protected Mock<IConfigureAlarm> MockAlarmService;
    }
    
}
