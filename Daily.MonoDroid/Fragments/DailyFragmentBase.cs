﻿
using System;

using Android.Support.V4.App;
using Android.Runtime;

namespace Daily.MonoDroid.Fragments
{
    public abstract class DailyFragmentBase : Fragment
    {
        public DailyFragmentBase()
        {
            
        }

        public DailyFragmentBase(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
            
        }
    }
}

