﻿using System;
using Android.Content;

namespace Daily.MonoDroid.Views
{
    public static class Extensions
    {
        
        public static int PixelToDp(this Context context, int px)
        {
            var density = context.Resources.DisplayMetrics.Density;
            return (int)(px * density + 0.5f);

        }
    }
}

