﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Converters;
using Daily.Core.ViewModels;
using Daily.Models;
using Daily.Web.Services;

namespace Daily.Web.Controllers.Controllers
{
    public class NullGuidConverter : JsonConverter
    {
        #region implemented abstract members of JsonConverter

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return Guid.NewGuid();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Guid);
        }

        #endregion

    }

    public class DateJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;
            response.ContentEncoding = ContentEncoding ?? response.ContentEncoding;
            if (Data != null)
            {
                var converter = new IsoDateTimeConverter();
                converter.DateTimeFormat = "yyyy-MM-dd";
                response.Write(JsonConvert.SerializeObject(Data, converter));
            }
        }
    }

    public class TimeJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = "application/json";
            if (Data != null)
            {
                var converter = new IsoDateTimeConverter();
                converter.DateTimeFormat = "h:mm tt";
                response.Write(JsonConvert.SerializeObject(Data, converter));
            }
        }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWeekViewModel()//DateTime? date)
        {
            var userId = Guid.Empty;
            DateTime date;
            if (!DateTime.TryParse(Request["date"], out date))
            {
                date = DateTime.Today;
            }
            date = date.AddDays(-(int)date.DayOfWeek);
            var dayVMs = new List<DayViewModel>();
            for (int i = 0; i < 7; i++)
            {
                var dayVM = new DayViewModel(new StorageConnectionFactory(userId), new PopupFactory(userId), new ConfigureAlarm(userId));
                dayVM.Date = date.AddDays(i);
                dayVMs.Add(dayVM);
            }
            var result = new DateJsonResult
            {
                Data = dayVMs,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
            return result;
        }

        public ActionResult GetDayViewModel()//DateTime? date)
        {
            DateTime date;
            if (!DateTime.TryParse(Request["date"], out date))
            {
                return null;
            }
            var userId = Guid.Empty;
            var dayVM = new DayViewModel(new StorageConnectionFactory(userId), new PopupFactory(userId), new ConfigureAlarm(userId));
            dayVM.Date = date;
            var toDos = dayVM.GetToDos();
            var result = new TimeJsonResult
            {
                Data = toDos,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
            return result;
        }

        public ActionResult GetToDoViewModel()//DateTime? date, string toDoId)
        {
            string toDoId = Request["toDoId"];
            DateTime date;
            if (!DateTime.TryParse(Request["date"], out date) || string.IsNullOrEmpty(toDoId))
            {
                return null;
            }
            var userId = Guid.Empty;
            var toDoVM = new ToDoViewModel(new StorageConnectionFactory(userId),
                             new ConfigureAlarm(userId),
                             new PopupFactory(userId), null, null, null);
            toDoVM.NotifyDateTime = date;
            return Json(toDoVM, JsonRequestBehavior.AllowGet);
        }


        public ActionResult StoreToDo()
        {
            var userId = Guid.Empty;
            ToDo toDo;
            try
            {
                using (var stream = new StreamReader(Request.InputStream, Request.ContentEncoding))
                {
                    toDo = JsonConvert.DeserializeObject<ToDo>(stream.ReadToEnd(), new NullGuidConverter());

                    var bar = toDo;
                }
                var toDoVM = new ToDoViewModel(new StorageConnectionFactory(userId),
                                 new ConfigureAlarm(userId),
                                 new PopupFactory(userId), null, null, null);
                toDoVM.NotifyDateTime = toDo.NotifyDateTime.ToLocalTime();
                toDoVM.Save(toDo.NotifyDateTime);

                return Json(toDoVM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpException();
            }
        }

        public ActionResult DoToDo()
        {
            var userId = Guid.Empty;
            ToDo toDo;
            try
            {
                using (var stream = new StreamReader(Request.InputStream, Request.ContentEncoding))
                {
                    toDo = JsonConvert.DeserializeObject<ToDo>(stream.ReadToEnd());
                }

                var toDoVM = new ToDoViewModel(new StorageConnectionFactory(userId), 
                                 new ConfigureAlarm(userId),
                                 new PopupFactory(userId), null, null, null);
                toDoVM.NotifyDateTime = DateTime.Today;
                toDoVM.DoToDo(toDo.NotifyDateTime);

                return Json(toDoVM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpException();
            }
        }

        public ActionResult Day()
        {
            return View();
        }

        public ActionResult Week()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}

