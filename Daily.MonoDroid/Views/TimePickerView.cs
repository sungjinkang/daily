﻿using System;

using Android.Content;
using Android.Util;
using Android.Views;

namespace Daily.MonoDroid.Views
{
    public class TimePickerView : TimeView, GestureDetector.IOnGestureListener
    {
        public event EventHandler ChangeTimeStart;
        public event EventHandler ChangeTimeEnd;
        GestureDetector gestureDetector;
        int edit;


        public TimePickerView(Context context)
            : base(context)
        {
            Initialize();
        }

        public TimePickerView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public TimePickerView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            gestureDetector = new GestureDetector(this);
            DigitalClockView.Hour.SetBackgroundResource(Resource.Drawable.DigitSelector);
            DigitalClockView.Minute.SetBackgroundResource(Resource.Drawable.DigitSelector);
            DigitalClockView.TimeOfDay.SetBackgroundResource(Resource.Drawable.DigitSelector);

            DigitalClockView.Hour.Click += delegate
            {
                edit = 0;
                UpdateVisibility();
            };
            DigitalClockView.Minute.Click += delegate
            {
                edit = 1;
                UpdateVisibility();
            };
            DigitalClockView.TimeOfDay.Click += delegate
            {
                Time = Time.Hour >= 12 ? Time.AddHours(-12) : Time.AddHours(12);
            };


            for (int i = 0, j = 5; i < NumbersCount; i++, j += 5)
            {
                AnalogClockView.HourViews[i].SetBackgroundResource(Resource.Drawable.ClockNumberSelector);
                AnalogClockView.MinuteViews[i].SetBackgroundResource(Resource.Drawable.ClockNumberSelector);

            }

            AnalogClockView.Touch += (sender, e) =>
            {
                if (e.Event.ActionMasked == MotionEventActions.Up)
                {
                    InvokeChangeTimeEnd();
                }
                else
                {
                    var radius = AnalogClockView.Width / 2;
                    var x = e.Event.GetX();
                    var y = e.Event.GetY();
                    var deltaX = x - radius;
                    var deltaY = y - radius;
                        
                    var dist = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
                        
                    var min = radius - AnalogClockView.HourViews[0].Width;
                    var max = radius;
                        
                    good = dist >= min && dist <= max;
                    if (good)
                    {
                        var radians = Math.Atan2((double)(y - radius), (x - radius));
                        degrees = (float)(radians / Math.PI * 180);
                        if (degrees < 0)
                        {
                            degrees = 360 + degrees;
                        }
                        degrees += 75;
                        degrees %= 360;
                        
                        if (e.Event.ActionMasked == MotionEventActions.Up ||
                            e.Event.ActionMasked == MotionEventActions.Cancel ||
                            e.Event.GetX() < 0 || e.Event.GetY() < 0 ||
                            e.Event.GetX() > AnalogClockView.Width || e.Event.GetY() > AnalogClockView.Height)
                        {
                            InvokeChangeTimeEnd();
                        }
                        else if (longPress && e.Event.ActionMasked == MotionEventActions.Move)
                        {
                            TouchUpdate(true);
                        }
                    }
                }
                e.Handled = gestureDetector.OnTouchEvent(e.Event);
            };

            UpdateVisibility();
        }

        void TouchUpdate(bool fine)
        {
            if (edit == 0)
            {
                var position = ((int)(degrees / 30) + 1) % 12;
                var old = Time;
                if (old.Hour >= 12)
                {
                    position += 12;
                }
                Time = old.Date.AddHours(position).AddMinutes(old.Minute);
                if (!fine)
                {
                    NextEditState();
                }
            }
            else
            {
                if (fine)
                {
                    var old = Time;
                    Time = old.Date.AddHours(old.Hour).AddMinutes(((degrees + 15) / 6) % 60);
                }
                else
                {
                    var position = ((int)(degrees / 30) + 1) % 12;
                    var old = Time;
                    Time = old.Date.AddHours(old.Hour).AddMinutes((position * 5) % 60);
                }
            }
        }

        bool good;
        float degrees;

        void InvokeChangeTimeEnd()
        {
            longPress = false;
            var handler = ChangeTimeEnd;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        void NextEditState()
        {
            edit++;
            edit %= 2;
        
            UpdateVisibility();
        }


        void UpdateVisibility()
        {
            if (edit == 0)
            {
                AnalogClockView.ShowHours();
                AnalogClockView.SelectHourHand();

                DigitalClockView.Hour.Selected = true;
                DigitalClockView.Minute.Selected = false;
            }
            else
            {
                AnalogClockView.ShowMinutes();
                AnalogClockView.SelectMinuteHand();

                DigitalClockView.Hour.Selected = false;
                DigitalClockView.Minute.Selected = true;
            }
        }


        public override DateTime Time
        {
            get
            {
                return base.Time;
            }
            set
            {
                base.Time = value;
                AnalogClockView.Time = value;
                DigitalClockView.Time = value;
            }
        }

        public bool OnDown(MotionEvent e)
        {
            
            return true;
        }

        public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            return false;
        }

        bool longPress;
        public void OnLongPress(MotionEvent e)
        {
            var handler = ChangeTimeStart;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
            longPress = true;
            TouchUpdate(true);
        }

        public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            return false;
        }

        public void OnShowPress(MotionEvent e)
        {
        }

        public bool OnSingleTapUp(MotionEvent e)
        {
            if (good)
            {
                TouchUpdate(false);
            }
            return true;
        }
    }
}

