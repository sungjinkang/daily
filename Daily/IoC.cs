﻿using System;
using System.Collections.Generic;

namespace Daily
{
    public static class IoC
    {
        static readonly Dictionary<Type, Lazy<object>> _container = new Dictionary<Type, Lazy<object>>();

        public static void Register<T>(Lazy<object> obj)
        {
            _container[typeof(T)] = obj;
        }

        public static T Resolve<T>()
        {
            return (T)_container[typeof(T)].Value;
        }

        public static void Unregister<T>()
        {
            _container.Remove(typeof(T));
        }
    }
}

