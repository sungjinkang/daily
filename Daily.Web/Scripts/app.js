﻿var dailydidsApp = angular.module('dailydidsApp', [
    'ngRoute',
    'dailydidsControllers'
]);


dailydidsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/:date', {
                controller: 'DayViewController',
                templateUrl: function(params) { return '/Day/' + params.date; }
            }).
            otherwise({
                redirectTo: function() {
                        var date = new Date();
                        return '/' + date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                    }
            });
    }]);