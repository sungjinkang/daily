﻿using System;
using System.Linq;
using System.Collections.Generic;
using Daily.Core.Mediators;

namespace Daily.Core.Adapters
{
    public class DayOfWeekAdapter : IModelAdapter<IMediator<bool, DayOfWeek>>
    {
        readonly List<IMediator<bool, DayOfWeek>> items;

        public DayOfWeekAdapter()
        {
            items = Enum.GetValues(typeof(DayOfWeek))
                        .Cast<DayOfWeek>()
                        .Select(d => (IMediator<bool, DayOfWeek>)new BooleanDayOfWeekMediator{ Item = d })
                        .ToList();
        }

        #region IModelAdapter implementation

        public IMediator<bool, DayOfWeek> Get(int i)
        {
            return items[i];
        }

        public int Count
        {
            get
            {
                return items.Count;
            }
        }

        #endregion
    }
}

