﻿using System;
using Android.App;
using Android.Content;
using Daily.Core.ViewModels;
using Daily.MonoDroid.Activities;
using Android.Media;
using Android.Support.V4.App;
using System.Linq;

namespace Daily.MonoDroid.Services
{
    [Service]
    public class AlertToDoService : IntentService
    {
        public const int NotificationTag = 1;

        protected override void OnHandleIntent(Intent intent)
        {
            var notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;
            if (notificationManager == null)
            {
                return;
            }
            var toDoId = intent.GetStringExtra("ToDoId");
            var dateTimeString = intent.GetStringExtra("DateTimeString");

            if (!string.IsNullOrEmpty(toDoId) && !string.IsNullOrEmpty(dateTimeString))
            {
                var dateTime = DateTime.Parse(dateTimeString);
                var vm = new DayViewModel(IoC.Resolve<IStorageConnectionFactory>(), IoC.Resolve<IPopupFactory>(), null);
                vm.Date = dateTime;
                var toDoViewModels = vm.GetToDos();
                var toDoVM = toDoViewModels.FirstOrDefault(tdvm => tdvm.ToDoId.ToString() == toDoId);
                if (toDoVM != null && !toDoVM.DidDone)
                {
                    var mainActivityIntent = new Intent(this, typeof(MainActivity));

                    var builder = new NotificationCompat.Builder(this);
                    builder.SetSmallIcon(Resource.Drawable.Notification);
                    builder.SetContentTitle("ToDo");
                    builder.SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification));
                    builder.SetVibrate(new []{ -1L });
                    builder.SetContentIntent(PendingIntent.GetActivity(this, 1, mainActivityIntent, PendingIntentFlags.UpdateCurrent));
                    builder.SetAutoCancel(true);
                    builder.SetContentText(toDoVM.Title);
                    builder.SetSubText(toDoVM.Description);

                    notificationManager.Notify(NotificationTag, builder.Build());

                    var doToDoIntent = new Intent(this, typeof(DoToDoActivity));
                    doToDoIntent.AddFlags(ActivityFlags.NewTask |
                        ActivityFlags.ExcludeFromRecents |
                        ActivityFlags.NoHistory
                    );
                    doToDoIntent.PutExtra("ToDoId", toDoVM.ToDoId.ToString());
                    doToDoIntent.PutExtra("DateTimeString", dateTimeString);
                    StartActivity(doToDoIntent);
                }
            }
        }
    }
}

