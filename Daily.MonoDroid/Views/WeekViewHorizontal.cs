﻿using System;
using Android.Views;
using Android.Content;
using Android.Util;
using Daily.Core.ViewModels;
using Android.Widget;
using Android.Support.V4.View;
using System.Collections.Generic;
using Daily.Core.Utilities;

namespace Daily.MonoDroid.Views
{
    public class WeekViewHorizontal : ViewPager
    {
        public event EventHandler<DaySelectedEventArgs> DaySelected;

        public WeekViewHorizontal(Context context)
            : base(context)
        {
            Initialize();
        }

        public WeekViewHorizontal(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        void Initialize()
        {
            SetBackgroundResource(Resource.Drawable.WeekViewBackground);
            Adapter = new WeekViewPagerAdapter(this);
        }

        DateTime date;
        public void SetDate(DateTime date)
        {   
            this.date = date;
            var sunday = date.AddDays(-(int)date.DayOfWeek);
            var weeks = ((sunday - DateTimeExtensions.FirstSunday).Days / 7);
            SetCurrentItem(weeks, false);
        }


        public void NotifyDataSetChanged()
        {
            if (Adapter != null)
            {
                Adapter.NotifyDataSetChanged();
            }
        }

        class WeekViewPagerAdapter : PagerAdapter
        {
            readonly WeekViewHorizontal weekView;
            static ViewGroup.LayoutParams ChildLayoutParams = 
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MatchParent, 
                    ViewGroup.LayoutParams.MatchParent);
            static LinearLayout.LayoutParams DayLayoutParams = 
                new LinearLayout.LayoutParams(0, 
                    ViewGroup.LayoutParams.MatchParent, 1f);
            readonly List<DayViewSmall> checkedDayViews = new List<DayViewSmall>();

            public WeekViewPagerAdapter(WeekViewHorizontal weekView)
            {
                this.weekView = weekView;
            }

            public override int GetItemPosition(Java.Lang.Object @object)
            {
                return PositionNone;
            }

            public override bool IsViewFromObject(View view, Java.Lang.Object @object)
            {
                return view == @object;
            }

            public override int Count
            {
                get
                {
                    return DateTimeExtensions.WeeksCount;
                }
            }

            public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
            {
                var view = new LinearLayout(container.Context)
                {
                    LayoutParameters = ChildLayoutParams,
                    Orientation = Orientation.Horizontal,
                };

                var dayVMs = new DayViewModel[7];
                for (int i = 0; i < 7; i++)
                {
                    var dayView = new DayViewSmall(container.Context)
                    {
                        Section = position,
                        Index = i,
                        LayoutParameters = DayLayoutParams,
                        ViewModel = new DayViewModel(IoC.Resolve<IStorageConnectionFactory>(), IoC.Resolve<IPopupFactory>(), null),
                    };
                    var date = DateTimeExtensions.FirstSunday.AddDays((position * 7) + i);
                    dayView.SetDate(date);
                    dayView.Checked = weekView.date == dayView.ViewModel.Date;
                    dayView.Click += OnWeekViewItemClicked;
                    if (dayView.Checked)
                    {
                        checkedDayViews.Add(dayView);
                    }
                    view.AddView(dayView);
                }
                container.AddView(view);

                return view;
            }

            public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object)
            {
                var view = @object as View;
                if (view != null)
                {
                    container.RemoveView(view);
                    view.Dispose();
                }
            }

            void OnWeekViewItemClicked(object sender, EventArgs e)
            {
                if (!weekView.Enabled)
                {
                    return;
                }
                var dayView = sender as DayViewSmall;
                var daySelected = weekView.DaySelected;
                if (dayView != null && daySelected != null)
                {
                    foreach (var v in checkedDayViews)
                    {
                        v.Checked = false;
                    }
                    checkedDayViews.Clear();
                    dayView.Checked = true;
                    checkedDayViews.Add(dayView);

                    daySelected(weekView, new DaySelectedEventArgs
                        {
                            DateTime = dayView.ViewModel.Date,
                        });
                }
            }
            
        }


    }
}

