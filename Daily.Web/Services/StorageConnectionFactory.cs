﻿using System;

namespace Daily.Web.Services
{
    public class StorageConnectionFactory : IStorageConnectionFactory
    {
        Guid userId;
        public StorageConnectionFactory(Guid userId)
        {
            this.userId = userId;
        }
        public IStorageConnection Create()
        {
            return new StorageConnection(userId);
        }
    }
}

