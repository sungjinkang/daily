﻿using System;

namespace Daily.Core
{
    public class GuidEventArgs : EventArgs
    {
        public GuidEventArgs(Guid value)
        {
            Value = value;
        }

        public Guid Value
        {
            get;
            private set;
        }
    }
}

