﻿
namespace Daily
{
    public interface IAlert
    {
        string Path
        {
            get;
        }

        void SetAlert();

        void Play();
    }
}

