﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<div ng-controller="DayViewController" class="container">
    <ul>
        <li ng-repeat="toDo in selectedDayViewModel.Model">
            <div class="todo_view">
                <p>{{toDo.NotifyDateTime}}</p>
                <span>{{toDo.Title}}</span>
                <p>{{toDo.Description}}</p>
            </div>
        </li>
    </ul>
</div>