﻿using System;

using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.App;
using Daily.Core.ViewModels;
using System.Collections.Generic;
using Android.Graphics;
using System.Threading.Tasks;

namespace Daily.MonoDroid.Views
{
    public class DayViewSmall : DayViewBase, ICheckable
    {
        TextView dateTextView;
        TextView dayOfWeek;
        LinearLayout toDosContainer;

        public DayViewSmall(Context context)
            : base(context)
        {
            Initialize();
        }

        public DayViewSmall(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public DayViewSmall(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }


        void Initialize()
        {
            var view = Inflate(Context, Resource.Layout.DayViewSmall, this);
            dateTextView = view.FindViewById<TextView>(Resource.Id.Date);
            dayOfWeek = view.FindViewById<TextView>(Resource.Id.DayOfWeek);
            toDosContainer = view.FindViewById<LinearLayout>(Resource.Id.ToDosContainer);
            SetBackgroundResource(Resource.Drawable.DayViewSelector);
        }

        protected override void UpdateToDoViews(IList<ToDoViewModel> toDoViewModels)
        {
            toDosContainer.RemoveAllViews();
            var count = toDoViewModels.Count;
            if (count > 5)
            {
                toDosContainer.WeightSum = count;
            }
            foreach (var toDoVM in toDoViewModels)
            {
                var view = new View(Context)
                {
                    LayoutParameters = ToDoLayoutParams,
                };
                if (toDoVM.DidDone)
                {
                    view.SetBackgroundResource(Resource.Drawable.SmallToDoViewOutline);
                }
                else
                {
                    var drawable = Context.Resources.GetDrawable(Resource.Drawable.SmallToDoViewBackground);
                    drawable.SetColorFilter(toDoVM.Color.ToColor(), PorterDuff.Mode.Multiply);
                    view.SetBackgroundDrawable(drawable);
                }
                toDosContainer.AddView(view);
            }
            PostInvalidate();
        }

        protected override void OnViewmodelChanged(DayViewModel value)
        {
            base.OnViewmodelChanged(value);
            UpdateText();
        }


        static ViewGroup.LayoutParams ToDoLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 0, 1)
        {
            BottomMargin = 2,
        };

        protected void UpdateText()
        {
            var date = ViewModel.Date;
            dateTextView.Text = date.ToString("dd");
            dayOfWeek.Text = date.ToString("ddd");
        }


        protected override void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var activity = Context as Activity;
            if (activity != null)
            {
                activity.RunOnUiThread(() =>
                    {
                        base.OnViewModelPropertyChanged(sender, e);
                        switch (e.PropertyName)
                        {
                            case "Date":
                                UpdateText();
                                break;
                        }
                    });
            }
        }

        public override Android.Animation.StateListAnimator StateListAnimator
        {
            get
            {
                return base.StateListAnimator;
            }
            set
            {
                System.Diagnostics.Debug.WriteLine(0);
                base.StateListAnimator = value;
            }
        }
        static int[] CheckedState = { Android.Resource.Attribute.StateChecked };
        protected override int[] OnCreateDrawableState(int extraSpace)
        {
            var drawStates = base.OnCreateDrawableState(extraSpace + 1);
            if (Checked)
            {
                MergeDrawableStates(drawStates, CheckedState);
            }
            return drawStates;
        }

        public void Toggle()
        {
            Checked = !Checked;
        }

        bool @checked;
        public bool Checked
        {
            get
            {
                return @checked;
            }
            set
            {
                @checked = value;
                RefreshDrawableState();
            }
        }
    }
}

