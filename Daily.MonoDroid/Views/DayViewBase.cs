﻿using System;
using Android.Content;
using Android.Util;
using Daily.Core.ViewModels;
using Android.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;

namespace Daily.MonoDroid.Views
{
    public abstract class DayViewBase : RelativeLayout
    {
        DayViewModel viewModel;

        protected DayViewBase(Context context)
            : base(context)
        {
            Initialize();
        }

        protected DayViewBase(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        protected DayViewBase(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
        }

        public void SetDate(DateTime date)
        {
            viewModel.Date = date;
            Task.Run(() =>
                {
                    var items = ViewModel.GetToDos();
                    ((Activity)Context).RunOnUiThread(() => UpdateToDoViews(items));
                });
        }

        public DayViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                if (viewModel != null)
                {
                    viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                }
                viewModel = value;
                OnViewmodelChanged(value);
                if (value != null)
                {
                    value.PropertyChanged += OnViewModelPropertyChanged;
                }
            }
        }

        protected virtual void UpdateToDoViews(IList<ToDoViewModel> toDoViewModels)
        {

        }

        protected virtual void OnViewmodelChanged(DayViewModel value)
        {
        }

        protected virtual void OnViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();
            if (viewModel != null)
            {
                viewModel.PropertyChanged += OnViewModelPropertyChanged;
            }
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();
            if (viewModel != null)
            {
                viewModel.PropertyChanged += OnViewModelPropertyChanged;
            }
        }

        public int Section
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }
    }
}

