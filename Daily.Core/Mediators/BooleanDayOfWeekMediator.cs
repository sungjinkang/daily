﻿using System;

namespace Daily.Core.Mediators
{
    public class BooleanDayOfWeekMediator : IMediator<bool, DayOfWeek>
    {
        #region IMediator implementation

        public bool Value
        {
            get;
            set;
        }

        public DayOfWeek Item
        {
            get;
            set;
        }

        #endregion
    }
}

