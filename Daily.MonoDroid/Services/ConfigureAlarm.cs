using Android.Content;
using Daily.Models;
using Android.App;
using Android.OS;
using Java.Util;
using System;
using Daily.Core.ViewModels;
using System.Linq;
using Daily.Utilities;

namespace Daily.MonoDroid.Services
{
    public class ConfigureAlarm : IConfigureAlarm
    {
        readonly Context context;
        readonly IStorageConnectionFactory storageConnectionFactory;
        readonly IPopupFactory popupFactory;

        public ConfigureAlarm(Context context, IStorageConnectionFactory storageConnectionFactory, IPopupFactory popupFactory)
        {
            this.context = context;
            this.storageConnectionFactory = storageConnectionFactory;
            this.popupFactory = popupFactory;
        }

        #region IAlarmService implementation

        public void UpdateAlarms()
        {
            var todayDateTime = DateTime.Now;
            var dayViewModel = new DayViewModel(storageConnectionFactory, popupFactory, null);
            dayViewModel.Date = todayDateTime;
            var todayToDoViewModels = dayViewModel.GetToDos();

            var tomorrowDateTime = DateTime.Today.AddDays(1);
            dayViewModel.Date = tomorrowDateTime;
            var tomorrowToDoViewModels = dayViewModel.GetToDos();

            var alarmManager = context.GetSystemService(Context.AlarmService) as AlarmManager;
            if (alarmManager != null)
            {
                var alertToDoService = new Intent(context, typeof(AlertToDoService));
                var todayToDos = todayToDoViewModels.Where(tdvm => tdvm.NotifyDateTime.TimeOfDay >= todayDateTime.TimeOfDay).ToList();
                foreach (var toDo in todayToDos)
                {
                    SetAlarm(toDo.ToDoId.ToString(), toDo.AlertId, todayDateTime.Date.Add(toDo.NotifyDateTime.TimeOfDay), alarmManager, alertToDoService);
                }


                var tomorrowToDos = tomorrowToDoViewModels.Except(todayToDos, new ToDoViewModelEqualityComparer()).ToList();
                foreach (var toDo in tomorrowToDos)
                {
                    SetAlarm(toDo.ToDoId.ToString(), toDo.AlertId, tomorrowDateTime.Add(toDo.NotifyDateTime.TimeOfDay), alarmManager, alertToDoService);
                }
            }
        }

        void SetAlarm(string toDoId, int alertId, DateTime dateTime, AlarmManager alarmManager, Intent intent)
        {
            if (dateTime >= DateTime.Now)
            {
                var unixTime = Date.Parse(dateTime.ToString());
                intent.PutExtra("ToDoId", toDoId);
                intent.PutExtra("DateTimeString", dateTime.ToString());
                intent.PutExtra("AlertId", alertId);

                var pendingIntent = PendingIntent.GetService(context, alertId, intent, PendingIntentFlags.UpdateCurrent);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
                {
                    alarmManager.SetExact(AlarmType.RtcWakeup, unixTime, pendingIntent);
                }
                else
                {
                    alarmManager.Set(AlarmType.RtcWakeup, unixTime, pendingIntent);
                }
            }
        }

        public void UnsetAlarm(Guid toDoId)
        {
            var alarmManager = context.GetSystemService(Context.AlarmService) as AlarmManager;
            if (alarmManager != null)
            {
                using (var storage = storageConnectionFactory.Create())
                {
                    var toDo = storage.Fetch<ToDo>().FirstOrDefault(td => td.Id == toDoId);
                    if (toDo != null)
                    {
                        var intent = new Intent(context, typeof(AlertToDoService));
                        alarmManager.Cancel(PendingIntent.GetService(context, toDo.AlertId, intent, PendingIntentFlags.UpdateCurrent));
                        alarmManager.Cancel(PendingIntent.GetService(context, int.MaxValue - toDo.AlertId, intent, PendingIntentFlags.UpdateCurrent));
                    }
                }
            }
            UpdateAlarms();
        }

        public void SnoozeAlarm(Guid toDoId, int minutes)
        {
            var alarmManager = context.GetSystemService(Context.AlarmService) as AlarmManager;
            if (alarmManager != null)
            {
                using (var storage = storageConnectionFactory.Create())
                {
                    var toDo = storage.Fetch<ToDo>().FirstOrDefault(td => td.Id == toDoId);
                    if (toDo != null)
                    {
                        var intent = new Intent(context, typeof(AlertToDoService));
                        SetAlarm(toDo.Id.ToString(), int.MaxValue - toDo.AlertId, DateTime.Now.AddMinutes(minutes), alarmManager, intent);
                    }
                }
            }
            UpdateAlarms();
        }
        #endregion

    }

}

