﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.Views;
using Android.Content;
using Daily.MonoDroid;

namespace Daily.MonoDroid.Adapters
{
    public class RadioListAdapter<T> : BaseAdapter<T>
    {
        readonly IModelAdapter<T> modelAdapter;
        public RadioListAdapter(IModelAdapter<T> modelAdapter)
        {
            this.modelAdapter = modelAdapter;
            CurrentPosition = -1;
        }

        public int CurrentPosition
        {
            get;
            set;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ??
                       ((LayoutInflater)parent.Context.GetSystemService(Context.LayoutInflaterService))
                                .Inflate(Resource.Layout.EditToDoViewListItem, null, true);
            var radio = view.FindViewById<RadioButton>(Resource.Id.RadioIndicator);
            radio.Checked = position == CurrentPosition;

            radio.Tag = position;
            radio.Click -= OnRadioButtonClick;
            radio.Click += OnRadioButtonClick;
            view.FindViewById<TextView>(Resource.Id.Title).Text = this[position].ToString();
            return view;
        }

        static void OnRadioButtonClick(object sender, EventArgs e)
        {
            var radio = sender as RadioButton;
            if (radio != null)
            {
                ((ListView)radio.Parent.Parent).PerformItemClick((View)radio.Parent, (int)radio.Tag, (int)radio.Tag);
            }
        }

        public override int Count
        {
            get
            {
                return modelAdapter.Count;
            }
        }


        public override T this[int index]
        {
            get
            {
                return modelAdapter.Get(index);
            }
        }
    }
}

