﻿using Android.Graphics;

namespace Daily.MonoDroid.Views
{
    public static class ViewExtensions
    {
        public static string ToHexString(this Color color)
        {
            return string.Format("#{0}{1}{2}",
                color.R.ToString("X2"), 
                color.G.ToString("X2"),
                color.B.ToString("X2"));
        }

        public static Color ToColor(this string color)
        {
            if (string.IsNullOrEmpty(color))
            {
                return Color.White;
            }
            return Color.Rgb(
                int.Parse(color.Substring(1, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(color.Substring(3, 2), System.Globalization.NumberStyles.HexNumber),
                int.Parse(color.Substring(5, 2), System.Globalization.NumberStyles.HexNumber));
        }
    }
}

