using System;

namespace Daily
{
    public interface IStorageConnectionFactory
    {
        IStorageConnection Create();
    }

}

