﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<div ng-controller="WeekViewController" class="container">
    <table>
        <tr>
            <td ng-repeat="dayVM in dayViewModels" style="margin:0px">
                <div class="day_view_small" ng-class="selectedDayView(dayVM.DateTime)">
                    <p><a href="#{{dayVM.DateTime}}">{{dayVM.DayOfWeek}}</a></p>
                    <div style="margin:-1px;height:20px">
                        <ul ng-repeat="toDoVM in dayVM.ToDoViewModels" style="margin:0px;padding-left:0px;">
                            <li style="padding-bottom:5px;"><div class="todo_small"> </div></li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>