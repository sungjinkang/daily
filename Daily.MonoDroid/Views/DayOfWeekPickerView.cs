﻿using System;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Daily.MonoDroid.Views
{
    public class DayOfWeekPickerView : LinearLayout
    {
        public class DayCheckEventArgs : EventArgs
        {
            public DayOfWeek DayOfWeek
            {
                get;
                set;
            }

            public bool Checked
            {
                get;
                set;
            }
        }

        public event EventHandler<DayCheckEventArgs> DayChecked;
        CheckBox[] week = new CheckBox[7];
        ViewGroup weekContainer;
        public DayOfWeekPickerView(Context context)
            : base(context)
        {
            Initialize();
        }

        public DayOfWeekPickerView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public DayOfWeekPickerView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            var view = Inflate(Context, Resource.Layout.DayOfWeekPickerView, this);
            weekContainer = view.FindViewById<ViewGroup>(Resource.Id.WeekContainer);
            week[0] = view.FindViewById<CheckBox>(Resource.Id.Sunday);
            week[1] = view.FindViewById<CheckBox>(Resource.Id.Monday);
            week[2] = view.FindViewById<CheckBox>(Resource.Id.Tuesday);
            week[3] = view.FindViewById<CheckBox>(Resource.Id.Wednesday);
            week[4] = view.FindViewById<CheckBox>(Resource.Id.Thursday);
            week[5] = view.FindViewById<CheckBox>(Resource.Id.Friday);
            week[6] = view.FindViewById<CheckBox>(Resource.Id.Saturday);

            for (int i = 0; i < week.Length; i++)
            {
                week[i].Tag = i;
                week[i].CheckedChange += OnDayCheckedChange;
            }
        }

        public CheckBox[] Week
        {
            get
            {
                return week;
            }
        }

        void OnDayCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            var checkBox = sender as CheckBox;
            var dayChecked = DayChecked;
            if (checkBox != null && dayChecked != null)
            {
                dayChecked(this, new DayCheckEventArgs{ DayOfWeek = (DayOfWeek)(int)checkBox.Tag, Checked = checkBox.Checked });
            }
        }
    }
}

