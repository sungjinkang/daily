﻿using System;
using System.Runtime.Serialization;

namespace Daily.Core.ViewModels
{
    public abstract class ViewModel : NotifyPropertyChanged
    {
        protected ViewModel()
        {
        }

        [IgnoreDataMember]
        public Guid UserId
        {
            get;
            set;
        }

        string title;

        public virtual string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                InvokePropertyChanged();
            }
        }

        public override string ToString()
        {
            return title;
        }

        public virtual object Model
        {
            get;
            set;
        }
    }
}

