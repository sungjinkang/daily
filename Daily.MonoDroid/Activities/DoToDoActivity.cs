﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Daily.Core.ViewModels;
using Android.Media;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Daily.MonoDroid;
using Daily.MonoDroid.Views;
using Daily;
using Daily.MonoDroid.Adapters;
using Daily.MonoDroid.Services;

namespace Daily.MonoDroid.Activities
{
    [Activity(
        Theme = "@style/DailyThemeFullScreen", 
        NoHistory = true,
        ExcludeFromRecents = true,
        TaskAffinity = "")]
    public class DoToDoActivity : Activity
    {
        View ignore;
        View snooze;
        ToDoView toDoView;
        DidDoneView didDoneView;
        ToDoViewModel viewModel;
        MediaPlayer mediaPlayer;
        string error;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.AddFlags(
                WindowManagerFlags.Fullscreen |
                WindowManagerFlags.ShowWhenLocked |
                WindowManagerFlags.DismissKeyguard |
                WindowManagerFlags.TurnScreenOn);
            SetContentView(Resource.Layout.DoToDo);

            ignore = FindViewById<View>(Resource.Id.Ignore);
            ignore.Click += (sender, e) =>
            {
                Finish();
            };
            snooze = FindViewById<View>(Resource.Id.Snooze);
            snooze.Click += (sender, e) =>
            {
                Snooze();
                ClearNotifications();
                Finish();
            };

            var dateString = Intent.GetStringExtra("DateTimeString");
            var toDoId = Intent.GetStringExtra("ToDoId");

            if (string.IsNullOrEmpty(toDoId) || string.IsNullOrEmpty(dateString))
            {
                return;
            }

            var dateTime = DateTime.Parse(dateString);
            viewModel = new ToDoViewModel(IoC.Resolve<IStorageConnectionFactory>(), IoC.Resolve<IConfigureAlarm>(), IoC.Resolve<IPopupFactory>(), null, null, new AlertsAdapter(this));
            try
            {
                viewModel.ToDoId = new Guid(toDoId);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                return;
            }
            toDoView = FindViewById<ToDoView>(Resource.Id.ToDoView);
            toDoView.Date = dateTime;
            didDoneView = FindViewById<DidDoneView>(Resource.Id.DidDoneView);
            didDoneView.ViewModel = toDoView.ViewModel = viewModel;
            toDoView.Changed += (sender, e) =>
            {
                ClearNotifications();
                Finish();
            };


            if (viewModel.SelectedAlertIndex == 0 || viewModel.DidDone)
            {
                return;
            }

            try
            {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.SetAudioStreamType(Stream.Alarm);
                mediaPlayer.SetDataSource(this, Android.Net.Uri.Parse(viewModel.AlertsAdapter.Get(viewModel.SelectedAlertIndex).Path));
                mediaPlayer.Looping = true;
                mediaPlayer.Prepare();
                mediaPlayer.Start();
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        void Snooze()
        {
            if (viewModel != null)
            {
                var alarmManager = GetSystemService(Context.AlarmService) as AlarmManager;
                if (alarmManager != null)
                {
                    var snoozeKey = Resources.GetString(Resource.String.snooze);
                    var sharedPrefs = PreferenceManager.GetDefaultSharedPreferences(this);
                    var snoozeLength = sharedPrefs.GetInt(snoozeKey, 10);

                    new ConfigureAlarm(this, 
                        IoC.Resolve<IStorageConnectionFactory>(), 
                        IoC.Resolve<IPopupFactory>()).SnoozeAlarm(viewModel.ToDoId, snoozeLength);
                }
            }
        }

        void ClearNotifications()
        {
            var notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;
            if (notificationManager != null)
            {
                notificationManager.CancelAll();
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (!string.IsNullOrEmpty(error))
            {
                Toast.MakeText(this, error, ToastLength.Long).Show();
            }
        }

        public override void Finish()
        {
            base.Finish();
            if (mediaPlayer != null)
            {
                mediaPlayer.Stop();
            }
        }
    }
}