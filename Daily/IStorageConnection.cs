﻿using System;
using System.Linq;

namespace Daily
{
    public interface IStorageConnection : IDisposable
    {
        IQueryable<T> Fetch<T>() where T : class, new();

        void Delete<T>(T item);

        void Save<T>(T item);
    }
}

