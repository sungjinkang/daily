﻿using System;
using Daily.Models;
using System.Linq;
using System.Collections.Generic;

namespace Daily.Core.Adapters
{
    public class FrequencyAdapter : IModelAdapter<Frequency>
    {
        readonly List<Frequency> items;

        public FrequencyAdapter()
        {
            items = Enum.GetValues(typeof(Frequency))
                        .Cast<Frequency>()
                        .ToList();
        }
        #region IModelAdapter implementation
        public Frequency Get(int i)
        {
            return items[i];
        }
        public int Count
        {
            get
            {
                return items.Count;
            }
        }
        #endregion
        
    }
}

