﻿using System.Collections.Generic;
using Daily.Models;
using System;
using System.Linq;
using Daily.Core.Utilities;


namespace Daily.Core.ViewModels
{
    public class DayViewModel : ViewModel
    {
        readonly IStorageConnectionFactory storageConnectionFactory;
        readonly IPopupFactory popupFactory;
        readonly IConfigureAlarm configureAlarm;
        public DayViewModel(IStorageConnectionFactory storageConnectionFactory,
            IPopupFactory popupFactory,
            IConfigureAlarm configureAlarm)
        {
            this.popupFactory = popupFactory;
            this.storageConnectionFactory = storageConnectionFactory;
            this.configureAlarm = configureAlarm;
        }

        DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
                InvokePropertyChanged();
            }
        }

        public IList<ToDoViewModel> GetToDos()
        {
            try
            {
                using (var storage = storageConnectionFactory.Create())
                {
                    var toDos = storage.Fetch<ToDo>().Where(td => 
                        (td.DaysOff == 0 && !td.IsWeekly() && td.NotifyDateTime.Date <= date.Date) ||
                                    (td.DaysOff > 0 && td.IntermittentFrom(date)) ||
                                    (td.IsWeekly() && td.DayOfWeekIsSet(date.DayOfWeek)));
                    var didDones = storage.Fetch<DidDone>().Where(dd => dd.DoneDateTime.ToLocalTime().Date == date.Date);
                    var doneToDos = didDones.Join(toDos, dd => dd.ToDoId, td => td.Id, (dd, td) => td);
                    var normalToDos = toDos.Except(doneToDos);

                    return toDos.GroupJoin(didDones, td => td.Id, dd => dd.ToDoId, (td, dd) => dd
                        .Select(did => new ToDoViewModel(storageConnectionFactory, configureAlarm, popupFactory, null, null, null)
                            {
                                ToDoId = td.Id,
                                NotifyDateTime = td.NotifyDateTime,
                                Color = td.Color,
                                DidDone = true,
                            })
                        .DefaultIfEmpty(new ToDoViewModel(storageConnectionFactory, configureAlarm, popupFactory, null, null, null)
                            {
                                ToDoId = td.Id,
                                NotifyDateTime = td.NotifyDateTime,
                                Color = td.Color,
                            }))
                        .SelectMany(tdvm => tdvm)
                        .OrderBy(tdvm => tdvm.NotifyDateTime.TimeOfDay)
                        .ToList();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                return Enumerable.Empty<ToDoViewModel>().ToList();
            }
        }


    }

}