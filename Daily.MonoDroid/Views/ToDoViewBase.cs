
using System;

using Android.Content;
using Android.Util;
using Android.Widget;
using Daily.Core.ViewModels;
using System.ComponentModel;

namespace Daily.MonoDroid.Views
{

    public abstract class ToDoViewBase : RelativeLayout
    {
        public ToDoViewBase(Context context)
            : base(context)
        {
        }

        public ToDoViewBase(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        public ToDoViewBase(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
        }

        public DateTime Date
        {
            get;
            set;
        }

        ToDoViewModel viewModel;

        public ToDoViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
            set
            {
                if (viewModel != null)
                {
                    viewModel.PropertyChanged -= OnViewModelPropertyChanged;
                }
                viewModel = value;
                OnViewModelChanged(value);
                if (viewModel != null)
                {
                    viewModel.PropertyChanged += OnViewModelPropertyChanged;
                }
            }
        }

        protected virtual void OnViewModelChanged(ToDoViewModel value)
        {
        }

        public virtual void ClearListeners()
        {
        }

        protected abstract void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e);

    }
}
