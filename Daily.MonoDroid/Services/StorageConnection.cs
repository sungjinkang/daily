﻿using System;
using System.Linq;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
using System.IO;
using Daily.Models;
using SQLite.Net.Interop;

namespace Daily.MonoDroid.Services
{
    public class StorageConnection : IStorageConnection
    {
        SQLiteConnection conn;
        public StorageConnection()
        {
            var path = Path.Combine(Environment.GetFolderPath(
                               Environment.SpecialFolder.MyDocuments), "daily.db");
            conn = new SQLiteConnection(new SQLitePlatformAndroid(), path, SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.FullMutex);
            conn.CreateTable<ToDo>(CreateFlags.AllImplicit);
            conn.CreateTable<DidDone>(CreateFlags.AllImplicit);
        }

        #region IStorageConnection implementation

        public IQueryable<T> Fetch<T>() where T : class, new()
        {
            return conn.Table<T>().AsQueryable<T>();
        }

        public void Delete<T>(T item)
        {
            conn.Delete(item);
        }

        public void Save<T>(T item)
        {
            conn.InsertOrReplace(item);
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            conn.Close();
            conn.Dispose();
        }

        #endregion
    }
}

