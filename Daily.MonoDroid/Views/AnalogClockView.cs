﻿
using System;

using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Views.Animations;

namespace Daily.MonoDroid.Views
{
    public class AnalogClockView : ViewGroup
    {
        public event EventHandler TimeChanged;

        ViewStates minuteViewVisibilty;
        ViewStates hourViewVisibilty;
        public const int NumbersCount = 12;
        public TextView[] HourViews
        {
            get;
            private set;
        }
        public TextView[] MinuteViews
        {
            get;
            private set;
        }

        View clockFaceCenter;
        ImageView clockFaceOverlay;

        Animation animateShow = new ScaleAnimation(
                                    0f, 1f, 
                                    0f, 1f,
                                    Dimension.RelativeToSelf, 0.5f,
                                    Dimension.RelativeToSelf, 0.5f)
        {
            Duration = 300,
        };
        Animation animateHide = new ScaleAnimation(
                                    1f, 0f, 
                                    1f, 0f,
                                    Dimension.RelativeToSelf, 0.5f,
                                    Dimension.RelativeToSelf, 0.5f)
        {
            Duration = 300,
        };

        public AnalogClockView(Context context)
            : base(context)
        {
            Initialize();
        }

        public AnalogClockView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public AnalogClockView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        DateTime time;
        public virtual DateTime Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;


                if (HourViews[0] != null && MinuteViews[0] != null)
                {
                    for (int i = 0; i < NumbersCount; i++)
                    {
                        HourViews[i].Selected = false;
                        MinuteViews[i].Selected = false;
                        
                        if ((i + 1) % 12 == value.Hour % 12)
                        {
                            HourViews[i].Selected = true;
                        }
                        if (((i + 1) * 5) % 60 == value.Minute)
                        {
                            MinuteViews[i].Selected = true;
                        }
                    }
                }

                RequestLayout();

                var timeChanged = TimeChanged;
                if (timeChanged != null)
                {
                    timeChanged(this, EventArgs.Empty);
                }
            }
        }

        public View HourHand
        {
            get;
            private set;
        }

        public View MinuteHand
        {
            get;
            private set;
        }

        public int AnalogNumberSize
        {
            get;
            private set;
        }
        public int HourViewMeasureSpec
        {
            get;
            private set;
        }
        public ImageView ClockFace
        {
            get;
            private set;
        }
        void Initialize()
        {
            animateShow.AnimationEnd += OnAnimationEnd;
            animateHide.AnimationEnd += OnAnimationEnd;

            HourViews = new TextView[NumbersCount];
            MinuteViews = new TextView[NumbersCount];
            ClockFace = new ImageView(Context);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                ClockFace.SetImageResource(Resource.Drawable.ClockFace);
            }
            else
            {
                ClockFace.SetBackgroundResource(Resource.Drawable.CompatClockFace);
            }
            AddView(ClockFace);
            
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                clockFaceOverlay = new ImageView(Context);
                clockFaceOverlay.SetImageResource(Resource.Drawable.ClockFaceOverlay);
                var clockFaceColor = Resources.GetColor(Resource.Color.ClockFaceColor);
                clockFaceOverlay.SetColorFilter(clockFaceColor);
                AddView(clockFaceOverlay);
            }
            
            clockFaceCenter = new View(Context);
            clockFaceCenter.SetBackgroundResource(Resource.Drawable.ClockfaceCenter);
            AddView(clockFaceCenter);
            
            for (int i = 0, j = 5; i < HourViews.Length; i++, j += 5)
            {
                var view = HourViews[i] = new TextView(Context)
                {
                    Text = (i + 1).ToString(),
                    Gravity = GravityFlags.Center,
                };
                view.SetTextColor(Color.Black);
                AddView(view);

                MinuteViews[i] = new TextView(Context)
                {
                    Text = (j % 60).ToString("00"),
                };
                MinuteViews[i].Gravity = GravityFlags.Center;

                AddView(MinuteViews[i]);
            }
            HourHand = new View(Context);
            HourHand.SetBackgroundColor(Color.Black);
            
            MinuteHand = new View(Context);
            MinuteHand.SetBackgroundColor(Color.Black);
            
            AddView(HourHand);
            AddView(MinuteHand);

            Time = DateTime.Now;
            ShowHours();
        }

        public void ShowMinutes()
        {
            hourViewVisibilty = ViewStates.Invisible;
            minuteViewVisibilty = ViewStates.Visible;


            for (int i = 0; i < NumbersCount; i++)
            {
                HourViews[i].StartAnimation(animateHide);
                MinuteViews[i].StartAnimation(animateShow);
            }

        }

        public void ShowHours()
        {
            hourViewVisibilty = ViewStates.Visible;
            minuteViewVisibilty = ViewStates.Invisible;


            for (int i = 0; i < NumbersCount; i++)
            {
                HourViews[i].StartAnimation(animateShow);
                MinuteViews[i].StartAnimation(animateHide);
            }
        }

        public void SelectHourHand()
        {
            HourHand.SetBackgroundColor(Color.Red);
            MinuteHand.SetBackgroundColor(Color.Black);
            HourHand.BringToFront();
        }

        public void SelectMinuteHand()
        {
            HourHand.SetBackgroundColor(Color.Black);
            MinuteHand.SetBackgroundColor(Color.Red);
            MinuteHand.BringToFront();
        }

        void OnAnimationEnd(object sender, Animation.AnimationEndEventArgs e)
        {
            for (int i = 0; i < NumbersCount; i++)
            {
                HourViews[i].Visibility = hourViewVisibilty;
                MinuteViews[i].Visibility = minuteViewVisibilty;
            }    
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            var width = 200;
            var height = 200;
            var widthMode = MeasureSpec.GetMode(widthMeasureSpec);
            var widthSize = MeasureSpec.GetSize(widthMeasureSpec);
            if (widthMode == MeasureSpecMode.AtMost)
            {
                width = Math.Min(width, widthSize);
            }
            else if (widthMode == MeasureSpecMode.Exactly)
            {
                width = widthSize;
            }

            var heightMode = MeasureSpec.GetMode(heightMeasureSpec);
            var heightSize = MeasureSpec.GetSize(heightMeasureSpec);

            if (heightMode == MeasureSpecMode.AtMost)
            {
                height = Math.Min(height, heightSize);
            }
            else if (heightMode == MeasureSpecMode.Exactly)
            {
                height = heightSize;
            }

            var size = Math.Min(width, height);
            SetMeasuredDimension(size, size);
        }

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            double rads;
            int x = 0;
            int y = 0;

            AnalogNumberSize = Width / 5;
            var halfNumberSize = AnalogNumberSize / 2;

            var analogClockWidth = Width;
            var analogClockHeight = Height;
            var analogClockLeft = 0;
            var analogClockTop = 0;
            
            ClockFace.Measure(
                MeasureSpec.MakeMeasureSpec(analogClockWidth, MeasureSpecMode.Exactly),
                MeasureSpec.MakeMeasureSpec(analogClockHeight, MeasureSpecMode.Exactly));
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                clockFaceOverlay.Measure(
                    MeasureSpec.MakeMeasureSpec(analogClockWidth, MeasureSpecMode.Exactly), 
                    MeasureSpec.MakeMeasureSpec(analogClockHeight, MeasureSpecMode.Exactly));
            }
            
            ClockFace.Layout(
                analogClockLeft, 
                analogClockTop, 
                analogClockLeft + analogClockWidth * 4,
                analogClockTop + analogClockHeight * 4);
            HourViewMeasureSpec = MeasureSpec.MakeMeasureSpec(AnalogNumberSize, MeasureSpecMode.Exactly);
            
            
            var centerX = (analogClockWidth / 2) - halfNumberSize;
            var centerY = (analogClockHeight / 2) - halfNumberSize;
            var radius = centerX;

            //face
            for (int i = 0; i < HourViews.Length; i++)
            {
                var hourView = HourViews[i];
                hourView.Measure(HourViewMeasureSpec, HourViewMeasureSpec);
                hourView.SetTextSize(ComplexUnitType.Px, AnalogNumberSize / 2);
                rads = (i * 30d - 60d) / 180d * Math.PI;
                x = (int)((radius) * Math.Cos(rads)); 
                y = (int)((radius) * Math.Sin(rads));
                
                
                hourView.Layout(
                    analogClockLeft + centerX + x,
                    analogClockTop + centerY + y, 
                    analogClockLeft + centerX + x + AnalogNumberSize, 
                    analogClockTop + centerY + y + AnalogNumberSize);

                MinuteViews[i].Measure(HourViewMeasureSpec, HourViewMeasureSpec);
                MinuteViews[i].SetTextSize(ComplexUnitType.Px, hourView.TextSize);
                MinuteViews[i].Layout(
                    hourView.Left,
                    hourView.Top,
                    hourView.Right,
                    hourView.Bottom);
            }
            
            //center dot
            var quarterSize = halfNumberSize / 2;
            clockFaceCenter.Layout(
                analogClockLeft + centerX + halfNumberSize - quarterSize,
                analogClockTop + centerY + halfNumberSize - quarterSize,
                analogClockLeft + centerX + halfNumberSize + quarterSize,
                analogClockTop + centerY + halfNumberSize + quarterSize);
            

            //hour and minute hands
            var hourHandWidth = radius / 8;
            var minuteHandWidth = radius / 10;
            centerX += halfNumberSize - (minuteHandWidth / 2);
            centerY += halfNumberSize;
            
            //minute hand
            var analogMinute = time.Minute;
            var minuteLength = radius * 9 / 10;
            MinuteHand.Layout(
                analogClockLeft + centerX,
                analogClockTop + centerY - minuteLength, 
                analogClockLeft + centerX + minuteHandWidth, 
                analogClockTop + centerY);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                MinuteHand.PivotX = minuteHandWidth / 2;
                MinuteHand.PivotY = minuteLength;
                MinuteHand.Rotation = (analogMinute / 60f * 360f);
            }
            else
            {
                var rotate = new RotateAnimation(
                                 0f, 
                                 (analogMinute / 60f * 360f),
                                 Dimension.Absolute,
                                 minuteHandWidth / 2,
                                 Dimension.Absolute,
                                 minuteLength);
                rotate.FillBefore = true;
                rotate.FillAfter = true;
                MinuteHand.StartAnimation(rotate);
            }
            //hour hand
            centerX += (minuteHandWidth / 2) - (hourHandWidth / 2);
            var analogHour = time.Hour + (analogMinute / 60);
            var hourLength = radius * 3 / 5;
            HourHand.Layout(
                analogClockLeft + centerX,
                analogClockTop + centerY - hourLength, 
                analogClockLeft + centerX + hourHandWidth, 
                analogClockTop + centerY);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                HourHand.PivotX = hourHandWidth / 2;
                HourHand.PivotY = hourLength;
                HourHand.Rotation = (analogHour / 12f * 360f);
            }
            else
            {
                var rotate = new RotateAnimation(
                                 0f, 
                                 (analogHour / 12f * 360f),
                                 Dimension.Absolute,
                                 hourHandWidth / 2,
                                 Dimension.Absolute,
                                 hourLength);
                rotate.FillBefore = true;
                rotate.FillAfter = true;
                HourHand.StartAnimation(rotate);
            }
            
            //time of day indicator
            ClockFace.Layout(
                analogClockLeft, 
                analogClockTop,
                analogClockLeft + analogClockWidth,
                analogClockTop + analogClockHeight);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb)
            {
                ClockFace.Rotation = time.Hour / 24f * 360f;

                clockFaceOverlay.Layout(
                    analogClockLeft, 
                    analogClockTop,
                    analogClockLeft + analogClockWidth,
                    analogClockTop + analogClockHeight);
            }

        }

    }
}

