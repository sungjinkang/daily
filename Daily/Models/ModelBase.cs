﻿using System;

namespace Daily.Models
{
    public abstract class ModelBase
    {
        protected ModelBase()
        {
        }

        public Guid Id
        {
            get;
            set;
        }
    }
}

