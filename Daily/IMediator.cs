﻿using System;

namespace Daily
{
    public interface IMediator<TVALUE, TITEM>
    {
        TVALUE Value
        {
            get;
            set;
        }

        TITEM Item
        {
            get;
        }
    }
}

