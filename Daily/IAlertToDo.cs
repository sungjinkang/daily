﻿using System;
using Daily.Models;

namespace Daily
{
    public interface IAlertToDo
    {
        void Set(ToDo todo);

        void Unset(ToDo todo);
    }
}

