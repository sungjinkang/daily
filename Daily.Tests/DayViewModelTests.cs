﻿using System;
using NUnit.Framework;
using Moq;
using System.Linq;
using Daily.Models;
using System.Collections.Generic;
using Daily.Core.ViewModels;

namespace Daily.Tests
{

    [TestFixture]
    public class DayViewModelTests : ViewModelTests
    {
        DayViewModel dayViewModel;
        List<ToDo> toDos;
        List<DidDone> didDones;

        [SetUp]
        public void SetUp()
        {
            toDos = new List<ToDo>
            {
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
                    NotifyDateTime = new DateTime(1, 1, 3),
                },
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                    NotifyDateTime = new DateTime(1, 1, 3),
                },
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3),
                    NotifyDateTime = new DateTime(1, 1, 2),
                },
                new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4),
                    NotifyDateTime = new DateTime(1, 1, 4),
                }
            };
            didDones = new List<DidDone>
            {
                new DidDone
                {
                    ToDoId = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3),
                    DoneDateTime = new DateTime(1, 1, 3, 12, 0, 0),
                }
            };
            MockStorageConnectionFactory = new Mock<IStorageConnectionFactory>();

            MockStorageConnection = new Mock<IStorageConnection>();

            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(toDos.AsQueryable());
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());

            MockAlarmService = new Mock<IConfigureAlarm>();

            MockStorageConnectionFactory.Setup(s => s.Create()).Returns(MockStorageConnection.Object);
            MockPopupFactory = new Mock<IPopupFactory>();
            dayViewModel = new DayViewModel(MockStorageConnectionFactory.Object, MockPopupFactory.Object, MockAlarmService.Object);
        }

        [Test]
        public void GetToDosTest()
        {
            var dateTime = new DateTime(1, 1, 3);
            dayViewModel.Date = dateTime;
            var result = dayViewModel.GetToDos();

            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result.Count(tdvm => tdvm.DidDone));
            foreach (var item in result)
            {
                Assert.LessOrEqual(item.NotifyDateTime, dateTime);
            }
        }

        [Test]
        public void GetToDosTestBeforeAllToDoDays()
        {
            var dateTime = new DateTime(1, 1, 1);
            dayViewModel.Date = dateTime;
            var result = dayViewModel.GetToDos();

            Assert.AreEqual(0, result.Count);
            Assert.AreEqual(0, result.Count(tdvm => tdvm.DidDone));
        }

        [Test]
        public void GetToDosTestWhenOnlyEntryIsDone()
        {
            var id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            var toDoNotifyDate = new DateTime(2, 1, 1);
            var doneDateTime = new DateTime(2, 1, 1, 4, 1, 2);
            toDos = new List<ToDo>
            {
                new ToDo
                {
                    NotifyDateTime = toDoNotifyDate,
                    Id = id,
                }
            };
            didDones = new List<DidDone>
            {
                new DidDone
                {
                    DoneDateTime = doneDateTime,
                    ToDoId = id,
                    Id = new Guid(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                },
            };
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(toDos.AsQueryable());
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());
            dayViewModel.Date = toDoNotifyDate;
            var result = dayViewModel.GetToDos();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result.Count(tdvm => tdvm.DidDone));
        }

        [Test]
        public void GetToDosTestWeeklyFrequency()
        {
            var id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            var getDatetime = new DateTime(2015, 4, 20);
            var notifyDateTime = new DateTime(2015, 4, 19);
            toDos = new List<ToDo>
            {
                new ToDo
                {
                    NotifyDateTime = notifyDateTime,
                    Id = id,
                    Monday = true,
                },
                new ToDo
                {
                    NotifyDateTime = notifyDateTime,
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2),
                    Tuesday = true,
                }
            };
            didDones = new List<DidDone>
            {

            };
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(toDos.AsQueryable());
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());
            dayViewModel.Date = getDatetime;
            var result = dayViewModel.GetToDos();

            Assert.AreEqual(1, result.Count);
        }

        [Test]
        public void GetToDosTestIntermittentFrequency()
        {
            var id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
            var date = new DateTime(2015, 4, 20);
            toDos = new List<ToDo>
            {
                new ToDo
                {
                    NotifyDateTime = date,
                    Id = id,
                    DaysOn = 2,
                    DaysOff = 3,
                },
            };
            didDones = new List<DidDone>
            {

            };
            MockStorageConnection.Setup(s => s.Fetch<ToDo>()).Returns(toDos.AsQueryable());
            MockStorageConnection.Setup(s => s.Fetch<DidDone>()).Returns(didDones.AsQueryable());
            dayViewModel.Date = date;
            var result1 = dayViewModel.GetToDos();

            dayViewModel.Date = date.AddDays(1);
            var result2 = dayViewModel.GetToDos();

            dayViewModel.Date = date.AddDays(2);
            var result3 = dayViewModel.GetToDos();

            dayViewModel.Date = date.AddDays(3);
            var result4 = dayViewModel.GetToDos();

            dayViewModel.Date = date.AddDays(4);
            var result5 = dayViewModel.GetToDos();

            dayViewModel.Date = date.AddDays(5);
            var result6 = dayViewModel.GetToDos();


            Assert.AreEqual(1, result1.Count);
            Assert.AreEqual(1, result2.Count);
            Assert.AreEqual(0, result3.Count);
            Assert.AreEqual(0, result4.Count);
            Assert.AreEqual(0, result5.Count);
            Assert.AreEqual(1, result6.Count);
        }

        [Test]
        public void WhenGetIntermittentToDosForDateEarlierThanStartDate()
        {
            toDos.Clear();
            didDones.Clear();

            toDos.Add(new ToDo
                {
                    Id = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
                    NotifyDateTime = new DateTime(2015, 4, 26),
                    DaysOn = 2,
                    DaysOff = 1,
                });

            dayViewModel.Date = new DateTime(2015, 4, 25);
            var results = dayViewModel.GetToDos().ToList();

            Assert.AreEqual(0, results.Count);
        }
    }
}

