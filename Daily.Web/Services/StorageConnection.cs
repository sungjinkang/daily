﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Text;
using System.Collections.Generic;
using Daily.Models;
using System.Linq;

namespace Daily.Web.Services
{
    public class StorageConnection : IStorageConnection
    {
        MySqlConnection connection;
        Guid userId;

        public StorageConnection(Guid userId)
        {
            this.userId = userId;
            connection = new MySqlConnection(string.Format("Server={0};Database={1};User={2};Password={3};",
                    "localhost",
                    "DailyDb",
                    "public",
                    "public"));

            connection.Open();
            CreateTable<ToDo>();
            CreateTable<DidDone>();

        }

        Dictionary<Type, string> dotNetTypeToMySqlType = new Dictionary<Type, string>
        {
            { typeof(int), "INTEGER" },
            { typeof(long), "INTEGER" },
            { typeof(string), "TEXT" },
            { typeof(DateTime), "DATETIME" },
            { typeof(Guid), "CHAR(36)" },
            { typeof(bool), "TINYINT(1)" },
        };

        void CreateTable<T>()
        {
            var keyLength = Guid.Empty.ToString().Length;
            var props = typeof(T).GetProperties();
            var sb = new StringBuilder();
            foreach (var prop in props)
            {
                if (prop.Name == "Id")
                {
                    sb.AppendFormat(", {0} CHAR({1}) NOT NULL PRIMARY KEY", prop.Name, keyLength);
                }
                else if (prop.Name.Contains("Id") && prop.PropertyType == typeof(string))
                {
                    sb.AppendFormat(", {0} CHAR({1}) NOT NULL", prop.Name, keyLength);
                }
                else
                {
                    sb.AppendFormat(", {0} {1}",
                        prop.Name,
                        dotNetTypeToMySqlType[prop.PropertyType]);

                }
            }
            sb.Remove(0, 1);
            var query = string.Format("CREATE TABLE IF NOT EXISTS {0} ({1})", typeof(T).Name, sb);
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }


        public IQueryable<T> Fetch<T>() where T : class, new()
        {
            var props = typeof(T).GetProperties();
            var cols = new StringBuilder();
            foreach (var prop in props)
            {
                cols.AppendFormat(", {0}", prop.Name);
            }
            cols.Remove(0, 1);
            var query = string.Format("SELECT {0} FROM {1}", cols, typeof(T).Name);

            var result = new List<T>();
            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        object[] values = new object[props.Length];
                        reader.GetValues(values);
                        T obj = Activator.CreateInstance<T>();
                        int i = 0;
                        foreach (var prop in props)
                        {
                            if (prop.Name.Contains("Id") && prop.PropertyType == typeof(string))
                            {
                                prop.SetValue(obj, values[i].ToString());
                            }
                            else if (!(values[i] is DBNull))
                            {
                                prop.SetValue(obj, values[i]);
                            }
                            i++;
                        }
                        result.Add(obj);
                    }
                    reader.Close();
                }
            }
            return result.AsQueryable();
        }

        public void Delete<T>(T item)
        {
            var type = typeof(T);
            var query = string.Format("DELETE FROM {0} WHERE Id=@{1}", type.Name, type.Name);

            using (var command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                var result = command.ExecuteNonQuery();
            }
        }

        public void Save<T>(T item)
        {
            var props = item.GetType().GetProperties();
            var cols = new StringBuilder();
            var values = new StringBuilder();
            foreach (var prop in props)
            {
                cols.AppendFormat(", {0}", prop.Name);

                values.AppendFormat(", @{0}", prop.Name);
            }
            cols.Remove(0, 1);
            values.Remove(0, 1);
            var query = string.Format("REPLACE INTO {0} ({1}) VALUES ({2})", typeof(T).Name, cols, values);

            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;
                    foreach (var prop in props)
                    {
                        var value = prop.GetValue(item);
                        command.Parameters.Add(new MySqlParameter(string.Format("@{0}", prop.Name), value));
                    }
                    var result = command.ExecuteNonQuery();
                }
            }
            finally
            {
                connection.Close();
            }
        }

        #region IDisposable implementation

        public void Dispose()
        {
            connection.Close();
            connection.Dispose();
        }

        #endregion
    }
}

