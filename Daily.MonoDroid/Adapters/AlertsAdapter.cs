using System;
using Android.Content;
using Android.Media;
using System.Collections.Generic;
using Daily.Core;


namespace Daily.MonoDroid.Adapters
{
    public class AlertsAdapter : IModelAdapter<IAlert>
    {
        RingtoneManager ringtoneManager;
        Context context;
        IDictionary<int, IAlert> items;

        public AlertsAdapter(Context context)
        {
            this.context = context;
            ringtoneManager = new RingtoneManager(context);
            items = new Dictionary<int, IAlert>
            {
                { 0,new NoAlert() },
            };
        }
        #region IModelAdapter implementation
        public IAlert Get(int i)
        {
            if (!items.ContainsKey(i))
            {
                items[i] = new MediaData(context, ringtoneManager.GetRingtoneUri(i - 1));
            }
            return items[i];
        }
        public int Count
        {
            get
            {
                return ringtoneManager.Cursor.Count + 1;
            }
        }
        #endregion
    }



    class MediaData : IAlert
    {
        string title;
        string path;
        public MediaData(Context context, Android.Net.Uri uri)
        {
            Uri = uri;
            path = uri.ToString();
            var meta = new MediaMetadataRetriever();
            meta.SetDataSource(context, Uri);
            title = meta.ExtractMetadata(MetadataKey.Title);
        }

        public Android.Net.Uri Uri
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return title;
        }

        #region IAlert implementation

        public void SetAlert()
        {
            
        }

        public void Play()
        {
            
        }

        public string Path
        {
            get
            {
                return path;
            }
        }

        #endregion
    }

}

