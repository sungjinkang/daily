﻿ //////
 // Author: sungjkang@gmail.com
 //////

 //////
 // DAY VIEW
 //////
 function DayView() {
     var self = this;
     this.marginTop = 15;
     this.fontSize = 30;
     this.width = 768;
     this.view = $('<div>', { css: {
         'width' : this.width,
         'color': '#fff',
         'font-size': this.fontSize,
         '-webkit-touch-callout': 'none',
         '-webkit-user-select': 'none',
         '-khtml-user-select': 'none',
         '-moz-user-select': 'none',
         '-ms-user-select': 'none',
         'user-select': 'none',}});
    
     this.toDoViews = new Array();
     this.dateTime = new Date();
     this.count = 0;
    
     this.getView = function() {
         return self.view;
     }
        
     this.view.dblclick(function (e) {
         var toDoView = new ToDoView(self, { 'Model' :{
                 'Id': null,
                 'NotifyDateTime': self.dateTime,
                 'Title': null,
                 'Description': null,
                 'DaysOn': 0,
                 'DaysOff': 0,
             },
             'DidDone' : false});
         toDoView.setMode('new');
         self.view.append(toDoView.getView());
         toDoView.layout();
     });
 }

 DayView.prototype.height = 1200;

 DayView.prototype.top = function() {
     return this.view.position().top + this.marginTop;
 }

 DayView.prototype.setDelegate = function(delegate) {
     this.delegate = delegate;
 }

 DayView.prototype.newToDo = function() {
     var toDoView = new ToDoView(this, { 'Model' :{
             'Id': null,
             'NotifyDateTime': this.dateTime,
             'Title': null,
             'Description': null,
             'DaysOn': 0,
             'DaysOff': 0,
         },
         'DidDone' : false});
     toDoView.setMode('new');
     this.view.append(toDoView.getView());
     toDoView.layout();
 }

 DayView.prototype.layout = function() {
     var height = this.height / 24;
     for(var i = 0; i < this.toDoViews.length; i++) {
         this.toDoViews[i].layout();
     }
    
     for(var i = 0; i < this.toDoViews.length; i++) {
         var view = this.toDoViews[i];
         if (view.group === undefined) {
             view.group = new Array();
         }
         var viewTop = view.getView().position().top;
         var viewBottom = view.getView().height() + viewTop;
         for(var j = 0; j < this.toDoViews.length; j++) {
             if (i != j) {
                 var sibling = this.toDoViews[j];
                 var siblingTop = sibling.getView().position().top;
                 var siblingBottom = sibling.getView().height() + siblingTop;
                
                 if (viewTop <= siblingTop && viewBottom >= siblingTop) {
                     //collision
                     if (sibling.group === undefined) {
                         view.group.push(sibling);
                     }
                     sibling.group = view.group;
                 }
             }
         }
     }
    
    
     for(var i = 0; i < this.toDoViews.length; i++) {
         var view = this.toDoViews[i];
         if (!(view.group === undefined)) {
             if (view.group.length > 0) {
                 var width = view.getView().width() / (view.group.length + 1);
                 view.addLayout(0, width);
                 for(var j = 0, k = 1; j < view.group.length; j++, k++) {
                     view.group[j].addLayout(width * (j + k), width);
                     view.group.splice(j, 1);
                     j--;
                 }
             }
         }
     }
    
     for(var i = 0; i < this.toDoViews.length; i++) {
         var view = this.toDoViews[i];
         view.group = undefined;
     }
    
 }

 DayView.prototype.setToDoViewModels = function(toDoViewModels) {
     this.toDoViewModels = toDoViewModels;
    
     for(var i = 0; i < this.toDoViews.length; i++) {
         this.toDoViews[i].getView().remove();
     }
     this.toDoViews = new Array();
    
     for(var i = 0; i < toDoViewModels.length; i++) {
         var toDoView = new ToDoView(this, toDoViewModels[i]);
         this.toDoViews.push(toDoView);
         this.view.append(toDoView.getView());
     }
 }



 DayView.prototype.setDateTimeAsync = function(dateTime, callback) {
     var self = this;
     var view = this.view;
     this.dateTime = dateTime;
    
     $.ajax({
         url: 'FetchToDos',
         type: 'GET',
         data: { date: dateTime.toISOString() },
         beforeSend: function() {
             view.css({cursor: 'progress' });
         },
         complete: function() {
             view.css({cursor: 'default' });
             callback();
         },
         success: function(result) {
             for(var i = 0; i < result.length; i++) {
                 var dateString = result[i].NotifyDateTime;
                 result[i].NotifyDateTime = new Date(parseInt(dateString.substr(6)));
             }
             self.setToDoViewModels(result);
         },
         error: function() {
             alert('Catastrophic Failure.');
         }
     });
    
 }
    
 //////
 // TODO VIEW
 //////
 function ToDoView(dayView, toDoViewModel){
     var self = this;
     this.dayView = dayView;
     this.toDoViewModel = toDoViewModel;
     toDoViewModel.Model.NotifyDateTime = new Date(toDoViewModel.Model.NotifyDateTime);
     this.oldDateTime = toDoViewModel.Model.NotifyDateTime;
    
     this.view = $('<div/>', {
         class: 'todo_view',
         click: function(e) {
             e.stopPropagation();
         },
         dblclick: function(e) {
             e.stopPropagation();
         }
     });
    
     this.normalView = new NormalView(this, toDoViewModel);
     this.doneView = new DoneView(this, toDoViewModel);
     this.editView = new EditView(this, toDoViewModel);
     this.busyView = new BusyView();
     this.cancel = function() {
         if (this.mode == 'new') {
             this.getView().remove();
             return;
         }
         this.toDoViewModel.Model.NotifyDateTime = this.oldDateTime;
         if (toDoViewModel.DidDone) {
             this.setMode('done');
         } else {
             this.setMode('normal');
         }
         this.layout();
     }
    
     this.view.append(this.normalView.getView());
     this.view.append(this.doneView.getView());
     this.view.append(this.editView.getView());
     this.view.append(this.busyView.getView());
    
     this.setMode = function(mode) {
         self.normalView.getView().css({ display: (mode == 'normal' ? 'block' : 'none') });
         self.editView.getView().css({ display: (mode == 'edit' || mode == 'new' ? 'block' : 'none') });
         self.doneView.getView().css({ display: (mode == 'done' ? 'block' : 'none') });
         self.busyView.getView().css({ display: (mode == 'busy' ? 'block' : 'none') });
        
         this.mode = mode;
        
     };
    
     this.refresh = function() {
         if (self.mode == 'normal') {
             self.normalView.refresh();
         }
     }
    
     this.getView = function() {
         return self.view;
     }
    
     if (toDoViewModel.DidDone) {
         this.setMode('done');
     } else {
         this.setMode('normal');
     }
 }

 ToDoView.prototype.layout = function() {
     //var hourHeight = this.dayView.height / 24;
     //var top = this.toDoViewModel.Model.NotifyDateTime.getHours() * hourHeight;
     //top += this.toDoViewModel.Model.NotifyDateTime.getMinutes();
    
     //this.view.css({ top: top, position: 'absolute' });
 }

 ToDoView.prototype.addLayout = function(left, width) {
     this.view.css({ left: left, width: width });
 }

 //////
 // NORMAL VIEW
 //////    
 function NormalView(toDoView, toDoViewModel) {
     var self = this;
     this.titleView = $('<input/>', { class: 'form-control', value: toDoViewModel.Model.Title, click: function() {
         toDoView.setMode('edit');
     }});
     this.timeView = $('<div/>', { class: 'time', text: toDoViewModel.Model.NotifyDateTime.toDailyTimeString() }); 
     this.descriptionView = $('<div/>', { text: toDoViewModel.Model.Description });

     this.view = $('<div/>', { class: 'normal_state' })
         .append(this.timeView)
         .append($('<div/>', { class: 'form-group center-block', css: { padding: 20} })
             .append(this.titleView)
             .append(this.descriptionView)
             .append($('<br/>'))
             .append($('<button/>', { class: 'btn center-block', text: 'Done', click: function() {
                 $.ajax({
                     url: 'DoToDo',
                     data: JSON.stringify(toDoViewModel.Model),
                     dataType: 'json',
                     cache: false,
                     contentType: 'application/jsonrequest; charset=utf-8',
                     type: 'POST',
                     beforeSend: function() {
                         toDoView.setMode('busy');
                     },
                     success: function(result) {
                         if (result.DidDone) {
                             toDoView.setMode('done');
                         } else {
                             toDoView.setMode('normal');
                         }
                         toDoViewModel = result;
                     },
                     error: function() {
                         toDoView.setMode('normal');
                     }
                 });
             }}))
     );
     this.getView = function() {
         return self.view;
     }
    
     this.refresh = function() {
         self.descriptionView.val(toDoViewModel.Model.Description);
         self.titleView.val(toDoViewModel.Model.Title);
     }
 }

 //////
 // DONE VIEW
 //////
 function DoneView(toDoView, toDoViewModel) {
     var self = this;
     this.timeView = $('<div/>', { class: 'time', text: toDoViewModel.Model.NotifyDateTime.toDailyTimeString() }); 
     this.view = $('<div/>', { class: 'done_state' })
         .append(this.timeView)
         .append($('<div/>', { css: { 'padding': 20, 'margin': 20 }})
             .append($('<p/>', { text: 'Done' })));
        
        
     this.getView = function() {
         return self.view;
     }
 }

 //////
 // EDIT VIEW
 //////
 function EditView(toDoView, toDoViewModel) {
     var self = this;
     this.toDoViewModel = toDoViewModel;
     this.summaryView = new SummaryView(toDoViewModel);
     this.timeView = $('<div/>', { class: 'time', text: toDoViewModel.Model.NotifyDateTime.toDailyTimeString() }); 
     this.view = $('<div/>', { class: 'edit_state' })
         .append(this.timeView)
         .append($('<a/>', { class: 'time', text: 'cancel', css: { cursor: 'pointer', float: 'right', 'margin-right': 20, 'padding-top': 0 }, click: function() {
             toDoView.cancel();
         } }))
         .append($('<div/>', { class: 'form-group center-block', css: { padding: 20 } })
             .append($('<input/>', { class: 'form-control', placeholder: 'Title', value: toDoViewModel.Model.Title, change: function() {
                 toDoViewModel.Model.Title = this.value;
             }}))
             .append($('<textarea/>', { class: 'form-control', placeholder: 'Description', text: toDoViewModel.Model.Description, change: function() {
                 toDoViewModel.Model.Description = this.value;
             }}))
             .append($('<br/>'))
             .append($('<div/>', { class: 'input-group' })
                 .append($('<span/>', { class: 'input-group-addon', id: 'days-on-addon', text: 'On' }))
                 .append($('<input/>', { class: 'form-control', placeholder: 'Everyday', 'aria-describedby': 'days-on-addon', type: 'number', min: 0, value: self.checkInt(toDoViewModel.Model.DaysOn), change: function() {
                     self.summaryView.setDaysOn(this.value);
                 }}))
             )
             .append($('<div/>', { class: 'input-group' })
                 .append($('<span/>', { class: 'input-group-addon', id: 'days-off-addon', text: 'Off' }))
                 .append($('<input/>', { class: 'form-control', placeholder: 'Never', 'aria-describedby': 'days-off-addon' , type: 'number', min: 0, value: self.checkInt(toDoViewModel.Model.DaysOff), change: function() {
                     self.summaryView.setDaysOff(this.value);
                 }}))
             )
             .append($('<br/>'))
             .append(self.summaryView)
             .append($('<button/>', { class: 'btn center-block', text: 'Set', click: function() {
                     $.ajax({
                         url: 'StoreToDo',
                         data: JSON.stringify(toDoViewModel.Model),
                         dataType: 'json',
                         cache: false,
                         contentType: 'application/jsonrequest; charset=utf-8',
                         type: 'POST',
                         beforeSend: function() {
                             toDoView.setMode('busy');
                         },
                         success: function(result) {
                             toDoView.setMode('normal');
                         },
                         error: function() {
                             toDoView.setMode('edit');
                         },
                         complete: function() {
                             toDoView.refresh();
                         }
                     });
             }}))
         );
     self.summaryView.refreshText();

     this.getView = function() {
         return self.view;
     }
 }

 EditView.prototype.checkInt = function(n) {
     if (isNaN(n) || n <= 0) {
         return null;
     }
     return n;
 };
    
 EditView.prototype.refreshTimeString = function(n) {
     this.timeView.text(this.toDoViewModel.Model.NotifyDateTime.toDailyTimeString());
 }
    
 function SummaryView(toDoViewModel) {
     this.toDoViewModel = toDoViewModel;
     this.view = $('<div/>', {
         class: 'summary',
         text: 'Daily reminder at ' + toDoViewModel.Model.NotifyDateTime.toString(),
     });
 }
    


 SummaryView.prototype.setDaysOn = function(n) {
     this.toDoViewModel.Model.DaysOn = isNaN(n) ? 0 : n;
     this.refreshText();
 };
 SummaryView.prototype.setDaysOff = function(n) {
     this.toDoViewModel.Model.DaysOff = isNaN(n) ? 0 : n;
     this.refreshText();
 };
 SummaryView.prototype.refreshText = function() {
     var summaryString = 'Daily reminder at ' + this.toDoViewModel.Model.NotifyDateTime.toDailyTimeString();
     if (this.toDoViewModel.Model.DaysOn > 0) {
         summaryString += ' for the next ' + this.toDoViewModel.Model.DaysOn + ' days';
        
         if (this.toDoViewModel.Model.DaysOff > 0) {
             summaryString += ' and will resume after a ' + this.toDoViewModel.Model.DaysOff + ' day break';
         }
     }
     $(this).text(summaryString);
 };
    
 //////
 // BUSY VIEW
 //////
 function BusyView() {
     var self = this;
     this.view = $('<div/>', { class: 'busy_state' })
         .append($('<p/>', { text: 'loading' }));
     this.getView = function() {
         return self.view;
     }
 }


 //////
 // OTHER
 //////
 Date.prototype.toDailyTimeString = function() {
     var hour = this.getHours();
     var minutes = this.getMinutes();
     var timeOfDay = hour > 12 ? ' PM' : ' AM';
     if (hour == 0) {
         hour = 12;
     } else if (hour > 12) {
         hour %= 12;
     }
     if (minutes < 10) {
         minutes = '0' + minutes;
     }
    
     dateString = hour + ':' + minutes + timeOfDay;
     return dateString;
 }

 ////////
 // WEEK VIEW
 ////////
 function WeekView(weekViewModel, dayViewModels) {
     var self = this;
     this.viewModel = weekViewModel;
     this.dayViewModels = dayViewModels;
     var row = $('<tr/>');
     this.view = $('<table/>')
         .append(row);
     this.currentDayView = null;
        
     this.dayViewColumns = new Array();
     for (var i = 0; i < 7; i++) {
         var dayView = new DayViewSmall(dayViewModels[i]);

         (function(clickIndex){
             var column = $('<td/>', { click: function() {
                 for(var i = 0; i < self.dayViewColumns.length; i++) {
                     self.dayViewColumns[i].css({'background-color': '' });
                 }
                 $(this).css({ 'background-color': '#eef', 'border-top-left-radius': 20, 'border-top-right-radius': 20 });
                 self.delegate(dayViewModels[clickIndex]);
                
             }, css: { 'vertical-align': 'text-top' }})
                 .append($('<div/>')
                     .append(dayView.getView()));
                    
             row.append(column);
             dayView.column = column;
             self.dayViewColumns.push(column);
             self.currentDayView = dayView;
         })(i);
     }
     this.height = this.view.height();
     this.top = this.view.position().top;
     this.getView = function() {
         return self.view;
     }

 }

 WeekView.prototype.getCurrentDayView = function() {
     return this.currentDayView;
 }

 WeekView.prototype.setDelegate = function(delegate) {
     this.delegate = delegate;
 }

 WeekView.prototype.setToday = function() {
     var todayIndex = this.viewModel.TodayIndex;
     var today = this.dayViewModels[todayIndex];
     this.dayViewColumns[todayIndex].click();
     this.delegate();
 }

 ///////
 // DAY VIEW SMALL
 ///////
 function DayViewSmall(dayViewModel) {
     var self = this;
     this.selected = false;
    
     this.view = $('<div/>', { css: { color: '#888', padding: 10, 'text-align': 'center' }})
         .append($('<div/>')
             .append($('<p/>', { css: { padding: 0, margin: 0 }, text: dayViewModel.DayOfWeek }))
             .append($('<p/>', { css: { padding: 0, margin: 0 }, text: dayViewModel.Day })))
             .append($('<br/>'));
  
             for (var i = 0; i < dayViewModel.Count; i++) {
                
                 this.view.append($('<div/>', { class: 'todo_small',
                     css: { 'background-color': (dayViewModel.ToDoViewModels[i].DidDone ? '#89a' : '#eff') }}));
             }
                      
            
     this.height = this.view.height();
     this.top = this.view.position().top;
 }

 DayViewSmall.prototype.getView = function() {
     return this.view;
 }

