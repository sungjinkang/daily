﻿using Android.OS;
using Android.Preferences;
using Android.App;
using Android.Util;
using Android.Media;
using Daily.MonoDroid;

namespace Daily.MonoDroid.Activities
{
    [Activity(Label = "Settings",
        Theme = "@style/DailyTheme")]            
    public class SettingsActivity : PreferenceActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Settings);
            VolumeControlStream = Stream.Alarm;
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.Toolbar);
            toolbar.Title = "Settings";
            toolbar.Clickable = true;
            toolbar.Click += delegate
            {
                Finish();
            };
            var typedValue = new TypedValue();
            Theme.ResolveAttribute(Resource.Attribute.homeAsUpIndicator, typedValue, true);
            toolbar.SetNavigationIcon(typedValue.ResourceId);

            AddPreferencesFromResource(Resource.Xml.Settings);
        }
    }
}

