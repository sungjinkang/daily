
namespace Daily.Core
{

    public class NoAlert : IAlert
    {
        public void SetAlert()
        {
        }

        public void Play()
        {
        }

        public string Path
        {
            get
            {
                return string.Empty;
            }
        }

        public override string ToString()
        {
            return "No Alarm";
        }

        public int AlertId
        {
            get;
            set;
        }
    }

}
