
namespace Daily.Tests
{
    public class FakeAlert : IAlert
    {
        public long Id
        {
            get;
            set;
        }
        
        public string Path
        {
            get;
            set;
        }
        
        public void SetAlert()
        {
            AlertSet = true;
        }

        public bool AlertSet;


        public void Play()
        {
        }
    }
    
}
