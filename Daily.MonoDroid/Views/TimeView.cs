﻿using System;

using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Runtime;

namespace Daily.MonoDroid.Views
{
    public class TimeView : LinearLayout
    {
        public const int NumbersCount = 12;

        public event EventHandler TimeChanged;

        public TimeView(Context context)
            : base(context)
        {
            Initialize();
        }

        public TimeView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Initialize();
        }

        public TimeView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
            Inflate(Context, Resource.Layout.TimeView, this);
            DigitalClockView = FindViewById<DigitalClockView>(Resource.Id.DigitalClock);
            AnalogClockView = FindViewById<AnalogClockView>(Resource.Id.AnalogClock);
        }


        protected virtual void UpdateTimes()
        {
            AnalogClockView.Time = Time;
            DigitalClockView.Time = Time;
        }

        public AnalogClockView AnalogClockView
        {
            get;
            private set;
        }

        public DigitalClockView DigitalClockView
        {
            get;
            private set;
        }

        DateTime time;

        public virtual DateTime Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
                UpdateTimes();
                RequestLayout();

                var timeChanged = TimeChanged;
                if (timeChanged != null)
                {
                    timeChanged(this, EventArgs.Empty);
                }
            }
        }

        protected override IParcelable OnSaveInstanceState()
        {
            var timeViewSavedState = new TimeViewSavedState(base.OnSaveInstanceState())
            {
                Time = Time.ToString(),
            };
            return timeViewSavedState;
        }

        protected override void OnRestoreInstanceState(IParcelable state)
        {
            var timeViewSavedState = state as TimeViewSavedState;
            if (timeViewSavedState == null)
            {
                base.OnRestoreInstanceState(state);
                return;
            }
            base.OnRestoreInstanceState(timeViewSavedState.SuperState);
            DateTime savedTime;
            if (DateTime.TryParse(timeViewSavedState.Time, out savedTime))
            {
                Time = savedTime;
            }
        }

        class TimeViewSavedState : BaseSavedState
        {
            public TimeViewSavedState(Parcel source)
                : base(source)
            {
            
            }

            public TimeViewSavedState(IParcelable superState)
                : base(superState)
            {
                
            }

            public TimeViewSavedState(IntPtr javaReference, JniHandleOwnership transfer)
                : base(javaReference, transfer)
            {
                
            }

            public override void WriteToParcel(Parcel dest, ParcelableWriteFlags flags)
            {
                base.WriteToParcel(dest, flags);
                dest.WriteString(Time);
            }

            public string Time
            {
                get;
                set;
            }
        }
    }
}

