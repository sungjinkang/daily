﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html>
<html ng-app="dailydidsApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="sung j kang">
    <title>Daily Dids</title>
    <link href="../../Content/bootstrap.min.css" rel="stylesheet">
    <link href="../../Content/daily.css" rel="stylesheet">
    <script src="../../Scripts/angular.min.js"></script>
    <script src="../../Scripts/angular-route.js"></script>
    <script src="../../Scripts/app.js"></script>
    <script src="../../Scripts/controllers.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#/">Daily Dids</a>
            </div>
            <div class="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="javascript:AddToDo()">Add</a></li>
                </ul>
            </div>
        </div>
        <div ng-include="'Week'"></div>
    </nav>
    <div ng-view></div>
</body>
</html>
