using System;

namespace Daily
{
    public interface IPopupFactory
    {
        void Show(string message);
    }
}