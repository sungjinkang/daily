﻿using System;

using Android.App;
using Android.Views;
using Android.OS;
using Android.Support.V7.App;
using Daily.MonoDroid.Views;
using Daily.MonoDroid.Fragments;
using Android.Support.V7.Widget;

namespace Daily.MonoDroid.Activities
{
    [Activity(Label = "Daily Dids", 
        MainLauncher = true, 
        Icon = "@drawable/icon", 
        Theme = "@style/DailyTheme",
        WindowSoftInputMode = SoftInput.AdjustResize | SoftInput.StateHidden,
        LaunchMode = Android.Content.PM.LaunchMode.SingleInstance)]
    public class MainActivity : ActionBarActivity
    {
        public const string SelectedDateOfWeekKey = "SelectedDateOfWeek";
        public const string ToDoIdKey = "ToDoId";

        ViewGroup mainLayout;
        WeekViewHorizontal weekViewHorizontal;
        WeekViewVertical weekViewVertical;

        DateTime selectedDateOfWeek = DateTime.Today;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            VolumeControlStream = Android.Media.Stream.Alarm;

            mainLayout = FindViewById<ViewGroup>(Resource.Id.MainLayout);

            weekViewHorizontal = FindViewById<WeekViewHorizontal>(Resource.Id.WeekViewHorizontal);
            weekViewVertical = FindViewById<WeekViewVertical>(Resource.Id.WeekViewVertical);

            if (savedInstanceState == null)
            {
                SupportFragmentManager.BeginTransaction()
                    .Replace(Resource.Id.MainLayout, 
                    new OverviewFragment(), typeof(OverviewFragment).Name)
                    .Commit();
            }

            SupportActionBar.SetDisplayShowHomeEnabled(true);
            SupportActionBar.SetIcon(Resource.Drawable.PaddedIcon);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            base.OnRestoreInstanceState(savedInstanceState);
            var selectedDateOfWeekString = savedInstanceState.GetString("SelectedDateOfWeekString");
            var weekViewEnabled = savedInstanceState.GetBoolean("WeekViewEnabled", true);
            if (weekViewHorizontal != null)
            {
                weekViewHorizontal.Enabled = weekViewEnabled;
            }
            else if (weekViewVertical != null)
            {
                weekViewVertical.Enabled = weekViewEnabled;
            }

            if (selectedDateOfWeekString != null)
            {
                selectedDateOfWeek = DateTime.Parse(selectedDateOfWeekString);
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (weekViewHorizontal != null)
            {
                weekViewHorizontal.SetDate(selectedDateOfWeek);
                weekViewHorizontal.DaySelected += OnWeekViewDaySelected;
            }
            if (weekViewVertical != null)
            {
                weekViewVertical.SetDate(selectedDateOfWeek);
                weekViewVertical.DaySelected += OnWeekViewDaySelected;
            }
        }

        public void Reload()
        {
            RunOnUiThread(() =>
                { 
                    if (weekViewHorizontal != null)
                    {
                        weekViewHorizontal.NotifyDataSetChanged();
                    }
                    if (weekViewVertical != null)
                    {
                        weekViewVertical.NotifyDataSetChanged();
                    }
                });
        }

        public void SetWeekViewEnabled(bool enabled)
        {
            if (weekViewHorizontal != null)
            {
                weekViewHorizontal.Enabled = enabled;
            }
            if (weekViewVertical != null)
            {
                weekViewVertical.Enabled = enabled;
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            if (weekViewHorizontal != null)
            {
                weekViewHorizontal.DaySelected -= OnWeekViewDaySelected;
            }
            if (weekViewVertical != null)
            {
                weekViewVertical.DaySelected -= OnWeekViewDaySelected;
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutString("SelectedDateOfWeekString", selectedDateOfWeek.ToString());
            bool weekViewEnabled = true;
            if (weekViewHorizontal != null)
            {
                weekViewEnabled = weekViewHorizontal.Enabled;
            }
            else if (weekViewVertical != null)
            {
                weekViewEnabled = weekViewVertical.Enabled;
            }
            outState.PutBoolean("WeekViewEnabled", weekViewEnabled);
        }

        void OnWeekViewDaySelected(object sender, DaySelectedEventArgs e)
        {
            var overviewFragment = SupportFragmentManager
                .FindFragmentByTag(typeof(OverviewFragment).Name) as OverviewFragment;

            selectedDateOfWeek = e.DateTime;
            if (overviewFragment != null)
            {
                overviewFragment.SelectedDateOfWeek = e.DateTime;
            }

            var editFragment = SupportFragmentManager
                .FindFragmentByTag(typeof(EditToDoFragment).Name) as EditToDoFragment;
            if (editFragment != null)
            {
                editFragment.SetDate(e.DateTime);
            }
        }

    }
}


